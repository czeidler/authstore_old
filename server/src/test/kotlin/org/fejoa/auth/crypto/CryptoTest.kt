package org.fejoa.auth.crypto

import kotlinx.coroutines.experimental.runBlocking
import org.junit.Test
import kotlin.test.assertTrue


class CryptoTest {
    @Test
    fun testEKE2() {
        runBlocking {
            val secretKey = CryptoJava.deriveKeyPBKDF2WithSHA256("Password", CryptoHelper(CryptoJava).generateSalt16(), 10, SYM_ALGO.AES_CTR)
            val secret = CryptoJava.keyBytes(secretKey)

            val server = AuthProtocolEKE2_SHA3_256_CTR.createProver(CryptoJava, DH_GROUP.RFC5114_2048_256.name, secret)
            val encGx = server.getEncGX()
            val client = AuthProtocolEKE2_SHA3_256_CTR.createVerifier(CryptoJava, DH_GROUP.RFC5114_2048_256.name, secret,
                    encGx.first, encGx.second)
            val encGy = client.getEncGy()
            val serverFinish = server.setVerifierResponse(encGy.first, encGy.second, client.getAuthToken()) ?: throw Exception("Verifier denied access")
            assertTrue(client.verify(serverFinish.getAuthToken()))
        }
    }
}