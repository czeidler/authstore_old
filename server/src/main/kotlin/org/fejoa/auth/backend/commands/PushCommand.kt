package org.fejoa.auth.backend.commands

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.commons.codec.binary.Base64
import org.fejoa.auth.PUSH_METHOD
import org.fejoa.auth.PushArg
import org.fejoa.auth.backend.JsonHandler
import org.fejoa.auth.backend.SessionManager
import org.jetbrains.ktor.application.ApplicationCall


class PushCommand: JsonHandler(PUSH_METHOD) {
    suspend override fun handle(args: String?, call: ApplicationCall): Any {
        val pushArgs = ObjectMapper().readValue(args, PushArg::class.java)
        val user = SessionManager.getSession(call).getCurrentUser()
        val storage = user.getDataStorage(pushArgs.context) ?: throw Exception("Can't access storage")
        storage.put(pushArgs.branch, Base64.decodeBase64(pushArgs.data))
        return Unit
    }
}