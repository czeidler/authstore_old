package org.fejoa.auth.backend.commands

import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.auth.ADD_TO_INDEX_METHOD
import org.fejoa.auth.AddToIndexArg
import org.fejoa.auth.IndexEntry
import org.fejoa.auth.backend.JsonHandler
import org.fejoa.auth.backend.SessionManager
import org.jetbrains.ktor.application.ApplicationCall

class AddToIndexCommand: JsonHandler(ADD_TO_INDEX_METHOD) {
    suspend override fun handle(args: String?, call: ApplicationCall): Any {
        val addToIndexArgs = ObjectMapper().readValue(args, AddToIndexArg::class.java)
        val user = SessionManager.getSession(call).getCurrentUser()
        val index = user.getIndex() ?: return emptyList<IndexEntry>()
        index.add(addToIndexArgs.name, addToIndexArgs.branch)
        return Unit
    }
}