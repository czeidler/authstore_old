package org.fejoa.auth.backend.commands

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.commons.codec.binary.Base64
import org.fejoa.auth.PULL_METHOD
import org.fejoa.auth.PullArg
import org.fejoa.auth.PullResponseArg
import org.fejoa.auth.backend.JsonHandler
import org.fejoa.auth.backend.SessionManager
import org.jetbrains.ktor.application.ApplicationCall


class PullCommand: JsonHandler(PULL_METHOD) {
    suspend override fun handle(args: String?, call: ApplicationCall): Any {
        val pullArgs = ObjectMapper().readValue(args, PullArg::class.java)
        val user = SessionManager.getSession(call).getCurrentUser()
        val storage = user.getDataStorage(pullArgs.context) ?: throw Exception("Can't access storage")
        return PullResponseArg(Base64.encodeBase64String(storage.get(pullArgs.branch)))
    }
}