package org.fejoa.auth.backend.commands

import org.fejoa.auth.backend.JsonHandler
import org.fejoa.auth.GET_INDEX_METHOD
import org.fejoa.auth.GetIndexResponseArg
import org.fejoa.auth.backend.SessionManager
import org.jetbrains.ktor.application.ApplicationCall

class GetIndexCommand: JsonHandler(GET_INDEX_METHOD) {
    suspend override fun handle(args: String?, call: ApplicationCall): Any {
        val user = SessionManager.getSession(call).getCurrentUser()
        val index = user.getIndex() ?: return emptyList<String>()
        return GetIndexResponseArg(index.list().toTypedArray())
    }
}