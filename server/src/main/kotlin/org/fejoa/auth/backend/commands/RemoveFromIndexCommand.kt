package org.fejoa.auth.backend.commands

import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.auth.REMOVE_FROM_INDEX_METHOD
import org.fejoa.auth.RemoveFromIndexArg
import org.fejoa.auth.backend.JsonHandler
import org.fejoa.auth.backend.SessionManager
import org.jetbrains.ktor.application.ApplicationCall

class RemoveFromIndexCommand: JsonHandler(REMOVE_FROM_INDEX_METHOD) {
    suspend override fun handle(args: String?, call: ApplicationCall): Any {
        val removeFromIndexArgs = ObjectMapper().readValue(args, RemoveFromIndexArg::class.java)
        val user = SessionManager.getSession(call).getCurrentUser()
        val index = user.getIndex() ?: return emptyList<String>()
        index.remove(removeFromIndexArgs.name)
        return Unit
    }
}