package org.fejoa.auth.backend

import org.fejoa.auth.crypto.AuthProtocolEKE2_SHA3_256_CTR
import org.fejoa.auth.crypto.CryptoJava
import org.jetbrains.ktor.application.ApplicationCall
import org.jetbrains.ktor.sessions.session
import org.jetbrains.ktor.sessions.sessionOrNull
import java.io.File
import java.util.concurrent.ConcurrentHashMap
import javax.crypto.SecretKey


class Cookie(var fejoaAuthSessionId: String = "")

class Session {
    class User(val name: String, var authenticated: Boolean = false,
               var eke2Prover: AuthProtocolEKE2_SHA3_256_CTR<SecretKey>.ProverState0? = null)

    var currentUser: User? = null
        private set

    fun getOrCreateUser(name: String): User {
        synchronized(this) {
            currentUser?.let {
                return it
            }
            return User(name).let{
                currentUser = it
                it
            }
        }
    }

    fun unsetCurrentUser(): User? {
        return synchronized(this) {
            val oldUser = currentUser
            currentUser = null
            oldUser
        }
    }

    fun getUserListFileStorage(): UserListFileStorage {
        return UserListFileStorage(File("Users"))
    }

    fun getCurrentUser(): org.fejoa.auth.backend.User {
        val user = currentUser ?: throw Exception("Access denied")
        if (!user.authenticated)
            throw Exception("Not authenticated")
        return getUserListFileStorage().getUser(user.name)
    }
}

object SessionManager {
    private val sessionMap = ConcurrentHashMap<String, Session>()

    fun closeSession(call: ApplicationCall) {
        synchronized(this) {
            val cookie = call.sessionOrNull<Cookie>() ?: return
            sessionMap.remove(cookie.fejoaAuthSessionId)
        }
    }

    fun getSession(call: ApplicationCall): Session {
        synchronized(this) {
            val cookie = call.sessionOrNull<Cookie>() ?: return newSession(call)
            return sessionMap[cookie.fejoaAuthSessionId] ?: newSession(call)
        }
    }

    private fun newSessionId(): String {
        return CryptoJava.hex(CryptoJava.random(16))
    }

    private fun newSession(call: ApplicationCall): Session {
        val cookie = Cookie(newSessionId())
        val session = Session()
        sessionMap[cookie.fejoaAuthSessionId] = session
        call.session(cookie)
        return session
    }
}
