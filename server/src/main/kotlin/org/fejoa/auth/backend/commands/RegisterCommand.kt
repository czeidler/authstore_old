package org.fejoa.auth.backend.commands

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import org.fejoa.auth.KDFParams
import org.fejoa.auth.REGISTER_METHOD
import org.fejoa.auth.RegisterArg
import org.fejoa.auth.RegisterResponseArg
import org.fejoa.auth.backend.JsonHandler
import org.fejoa.auth.backend.KDFParamsDeserialzier
import org.fejoa.auth.backend.SessionManager
import org.fejoa.auth.backend.UserListFileStorage
import org.jetbrains.ktor.application.ApplicationCall
import java.io.File


class RegisterCommand : JsonHandler(REGISTER_METHOD) {
    suspend override fun handle(args: String?, call: ApplicationCall): Any {
        val mapper = ObjectMapper()
        val module = SimpleModule()
        module.addDeserializer(KDFParams::class.java, KDFParamsDeserialzier())
        mapper.registerModule(module)

        val registerArgs = mapper.readValue(args, RegisterArg::class.java)
        val session = SessionManager.getSession(call)
        val userList = session.getUserListFileStorage()
        if (userList.exists(registerArgs.user))
            throw Exception("Invalid user name")
        val authKey = registerArgs.authKey ?: throw Exception("Auth key missing")

        try {
            val user = userList.addUser(registerArgs.user)
            user.setAuthKey(authKey)
        } catch (e: Exception) {
            // clean up
            userList.delete(registerArgs.user)
            throw e
        }
        session.getOrCreateUser(registerArgs.user).authenticated = true
        return RegisterResponseArg(registerArgs.user, "Successfully registered")
    }
}