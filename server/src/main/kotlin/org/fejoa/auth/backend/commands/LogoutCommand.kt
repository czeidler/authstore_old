package org.fejoa.auth.backend.commands

import org.fejoa.auth.*
import org.fejoa.auth.backend.JsonHandler
import org.fejoa.auth.backend.SessionManager
import org.jetbrains.ktor.application.ApplicationCall


class LogoutCommand : JsonHandler(LOGOUT_METHOD) {
    suspend override fun handle(args: String?, call: ApplicationCall): Any {
        val removed = SessionManager.getSession(call).unsetCurrentUser()
        if (removed != null)
            return LogoutResponseArg("Bye bye ${removed.name}")
        else
            return LogoutResponseArg("Sorry you where not logged in")
    }

}