package org.fejoa.auth.backend

import org.jetbrains.ktor.application.ApplicationCall


abstract class JsonHandler(val name: String) {
    suspend abstract fun handle(args: String?, call: ApplicationCall): Any
}