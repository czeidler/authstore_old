package org.fejoa.auth.backend

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.TreeNode
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import java.io.IOException
import com.fasterxml.jackson.databind.module.SimpleModule
import org.fejoa.auth.KDFParams
import org.fejoa.auth.PDKDF2Params
import org.fejoa.auth.UserKey
import org.fejoa.auth.crypto.*


class KeepAsJsonDeserialzier : JsonDeserializer<String>() {
    @Throws(IOException::class, JsonProcessingException::class)
    override fun deserialize(jsonParser: JsonParser, context: DeserializationContext): String {
        val tree = jsonParser.codec.readTree<TreeNode>(jsonParser)
        return tree.toString()
    }
}

/**
 * To be used for deserialization
 */
class JsonRPCCallReceiver(var method: String? = null, var id: Int = -1) {
    var jsonrpc: String? = "2.0"
    @JsonDeserialize(using = KeepAsJsonDeserialzier::class)
    var params: String? = null
}


class KDFParamsDeserialzier : JsonDeserializer<KDFParams>() {
    @Throws(IOException::class, JsonProcessingException::class)
    override fun deserialize(jsonParser: JsonParser, context: DeserializationContext): KDFParams {
        val tree = jsonParser.codec.readTree<TreeNode>(jsonParser)
        val kdfAlgorithm = tree["algorithm"].traverse().nextTextValue()
        when (kdfAlgorithm) {
            KDF_ALGO.PBKDF2WithSHA256.name -> {
                return ObjectMapper().treeToValue(tree, PDKDF2Params::class.java)
            }
            else -> throw Exception("Unsupported KDF algorithm: $kdfAlgorithm")
        }
    }
}

fun parseUserKey(value: String): UserKey {
    val mapper = ObjectMapper()
    val module = SimpleModule()
    module.addDeserializer(KDFParams::class.java, KDFParamsDeserialzier())
    mapper.registerModule(module)
    return mapper.readValue(value, UserKey::class.java)
}

