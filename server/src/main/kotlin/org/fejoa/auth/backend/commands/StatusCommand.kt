package org.fejoa.auth.backend.commands

import org.fejoa.auth.STATUS_METHOD
import org.fejoa.auth.StatusResponseArg
import org.fejoa.auth.backend.JsonHandler
import org.fejoa.auth.backend.SessionManager
import org.jetbrains.ktor.application.ApplicationCall

class StatusCommand : JsonHandler(STATUS_METHOD) {
    suspend override fun handle(args: String?, call: ApplicationCall): Any {
        val user = SessionManager.getSession(call).currentUser ?: return StatusResponseArg()
        if (!user.authenticated)
            return StatusResponseArg()
        return StatusResponseArg(user.name)
    }
}