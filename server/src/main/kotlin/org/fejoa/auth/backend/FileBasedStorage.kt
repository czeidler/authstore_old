package org.fejoa.auth.backend

import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.auth.IndexEntry
import org.fejoa.auth.UserKey
import org.fejoa.auth.UserKeyParams
import java.io.DataOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.net.URLEncoder


val DATA_DIR = "data"

class UserFileStorage(val dir: File) : User {
    override fun setAuthKey(loginKey: UserKey) {
        val loginKeyFile = getLoginKeyFile()
        loginKeyFile.writeBytes(ObjectMapper().writeValueAsBytes(loginKey))
    }

    private fun getLoginKeyFile(): File {
        return File(dir, "loginKey.json")
    }

    private fun readLoginKey(): UserKey? {
        val loginKeyFile = getLoginKeyFile()
        if (!loginKeyFile.exists())
            return null
        val data = loginKeyFile.readText()
        return parseUserKey(data)
    }

    override fun getAuthKey(): String? {
        val userKey = readLoginKey()
        return userKey?.key
    }

    override fun getAuthParameters(): UserKeyParams? {
        val userKey = readLoginKey()
        return userKey?.params
    }

    override fun getIndex(): DataStorageIndex? {
        return DataStorageIndexFileStorage(dir)
    }

    override fun getDataStorage(context: String): DataStorage? {
        val contextDirName = URLEncoder.encode(context, "UTF-8")
        val dataDir = File(dir, DATA_DIR)
        return DataFileStorage(File(dataDir, contextDirName))
    }
}


class UserListFileStorage(val dir: File) : UserList {
    init {
        dir.mkdirs()
    }

    override fun exists(name: String): Boolean {
        return dir.list().contains(name)
    }

    private fun getUserDir(name: String): File {
        // convert to a valid file name
        val userDir = URLEncoder.encode(name, "UTF-8")
        return File(dir, userDir)
    }

    override fun addUser(name: String): User {
        if (exists(name))
            throw Exception("User already exist")

        val userDir = getUserDir(name)
        userDir.mkdirs()
        return UserFileStorage(userDir)
    }

    override fun getUser(name: String): User {
        if (!exists(name))
            throw Exception("User doesn't exist")
        return UserFileStorage(getUserDir(name))
    }

    override fun delete(name: String) {
       getUserDir(name).delete()
    }
}

class DataStorageIndexFileStorage(val dir: File) : DataStorageIndex {
    class Index {
        val branches = ArrayList<IndexEntry>()
    }

    val index: Index by lazy {
        try {
            ObjectMapper().readValue(getIndexFile(), Index::class.java)
        } catch (e: Exception) {
            // create new index
            Index()
        }
    }

    private fun getIndexFile(): File {
        val INDEX_NAME = "index.json"
        val dataDir = File(dir, DATA_DIR)
        return File(dataDir, INDEX_NAME)
    }

    private fun writeIndex() {
        val string = ObjectMapper().writeValueAsString(index)
        getIndexFile().writeText(string)
    }

    override fun list(): List<IndexEntry> {
        return index.branches
    }

    override fun add(name: String, branch: String) {
        index.branches.add(IndexEntry(name, branch))
        writeIndex()
    }

    override fun remove(name: String) {
        index.branches.removeIf { it.name == name }
    }
}

class DataFileStorage(val dataDir: File) : DataStorage {
    private fun validateKey(key: String) {
        if (key.findAnyOf(listOf("/", "..")) != null)
            throw Exception("Bad key: $key")
    }

    override fun get(key: String): ByteArray {
        validateKey(key)
        return File(dataDir, key).readBytes()
    }

    override fun put(key: String, data: ByteArray) {
        validateKey(key)
        dataDir.mkdirs()
        File(dataDir, key).writeBytes(data)
    }
}
