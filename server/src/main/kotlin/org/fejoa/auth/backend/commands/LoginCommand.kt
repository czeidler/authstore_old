package org.fejoa.auth.backend.commands

import org.fejoa.auth.EKE2LoginInitResponseArg
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.auth.EKE2LoginArg
import org.fejoa.auth.EKE2LoginFinishResponseArg
import org.fejoa.auth.LOGIN_METHOD
import org.fejoa.auth.backend.JsonHandler
import org.fejoa.auth.backend.SessionManager
import org.fejoa.auth.backend.UserListFileStorage
import org.fejoa.auth.crypto.*
import org.fejoa.auth.shared.EKEConsts
import org.jetbrains.ktor.application.ApplicationCall
import java.io.File


class LoginCommand : JsonHandler(LOGIN_METHOD) {
    suspend override fun handle(args: String?, call: ApplicationCall): Any {
        val loginArgs = ObjectMapper().readValue(args, EKE2LoginArg::class.java)
        if (loginArgs.protocol != EKEConsts.FEJOA_EKE2_SHA256)
            throw Exception("Protocol ${loginArgs.protocol} not supported")

        val userList = UserListFileStorage(File("Users"))
        if (!userList.exists(loginArgs.user))
            throw Exception("Invalid user name")
        val user = userList.getUser(loginArgs.user)

        when (loginArgs.state) {
            EKEConsts.STATE_INIT -> {
                val sessionUser = SessionManager.getSession(call).getOrCreateUser(loginArgs.user)
                sessionUser.eke2Prover = null
                val secret = user.getAuthKey() ?: throw Exception("Can't read auth key")
                val prover = AuthProtocolEKE2_SHA3_256_CTR.createProver(CryptoJava,
                        loginArgs.gpGroup, CryptoJava.fromBase64(secret))
                val encGx = prover.getEncGX()

                val authParams = user.getAuthParameters() ?: throw Exception("Can't read auth parameters")
                sessionUser.eke2Prover = prover
                return EKE2LoginInitResponseArg(authParams, CryptoJava.toBase64(encGx.first),
                        CryptoJava.toBase64(encGx.second))
            }
            EKEConsts.STATE_FINISH -> {
                val sessionUser = SessionManager.getSession(call).currentUser
                        ?: throw Exception("Invalid state (user not set)")
                val prover = sessionUser.eke2Prover ?: throw Exception("Invalid state (init state expected)")
                sessionUser.authenticated = false
                sessionUser.eke2Prover = null
                val encGy = loginArgs.encGY ?: throw Exception("Parameter encGy expected")
                val iv = loginArgs.iv ?: throw Exception("Parameter iv expected")
                val authToken = loginArgs.authToken ?: throw Exception("Parameter auth token expected")
                val prover1 = prover.setVerifierResponse(CryptoJava.fromBase64(encGy), CryptoJava.fromBase64(iv),
                        CryptoJava.fromBase64(authToken)) ?: throw Exception("Access denied")
                sessionUser.authenticated = true
                val proverAuthToken = prover1.getAuthToken()
                return EKE2LoginFinishResponseArg(CryptoJava.toBase64(proverAuthToken))
            }
            else -> throw Exception("Invalid state: ${loginArgs.state}")
        }
    }
}