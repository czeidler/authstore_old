package org.fejoa.auth.backend

import org.fejoa.auth.IndexEntry
import org.fejoa.auth.UserKey
import org.fejoa.auth.UserKeyParams


interface User {
    /**
     * Get a base64 encode version of the login key
     */
    fun getAuthKey(): String?
    fun getAuthParameters(): UserKeyParams?
    fun setAuthKey(loginKey: UserKey)

    fun getIndex(): DataStorageIndex?
    fun getDataStorage(context: String): DataStorage?
}

interface UserList {
    fun exists(name: String): Boolean
    fun addUser(name: String): User
    fun getUser(name: String): User
    fun delete(name: String)
}

interface DataStorageIndex {
    fun list(): List<IndexEntry>
    fun add(name: String, branch: String)
    fun remove(name: String)
}

interface DataStorage {
    fun get(key: String): ByteArray
    fun put(key: String, data: ByteArray)
}