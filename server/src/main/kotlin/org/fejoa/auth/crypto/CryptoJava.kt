package org.fejoa.auth.crypto

import org.apache.commons.codec.binary.Base64
import org.apache.commons.codec.binary.Hex
import java.security.MessageDigest
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec

object CryptoJava : CryptoInterface<SecretKey> {
    val secureRandom = SecureRandom()

    override fun isEqual(array1: ByteArray, array2: ByteArray): Boolean {
        return array1 contentEquals array2
    }

    override fun toBase64(data: ByteArray): String {
        return Base64.encodeBase64String(data)
    }

    override fun fromBase64(base64: String): ByteArray {
        return Base64.decodeBase64(base64)
    }

    override fun hex(data: ByteArray): String {
        return Hex.encodeHexString(data)
    }

    private fun getMessageDigest(algorithm: HASH_ALGO): MessageDigest {
        when (algorithm) {
            HASH_ALGO.SHA256 ->
                return MessageDigest.getInstance("SHA-256")
            else -> throw Exception("Unsupported hash algorithm: ${algorithm.name}")
        }
    }

    suspend override fun hash(data: ByteArray, algorithm: HASH_ALGO): ByteArray {
        return getMessageDigest(algorithm).digest(data)
    }

    override fun random(size: Int): ByteArray {
        val buffer = ByteArray(size)
        secureRandom.nextBytes(buffer)
        return buffer
    }

    suspend override fun secretKey(secret: ByteArray, symSettings: CryptoSettings.Symmetric): SecretKey {
        return SecretKeySpec(secret, 0, symSettings.keySize / 8, symSettings.keyType)
    }

    suspend override fun encryptSymmetric(plain: ByteArray, key: SecretKey, iv: ByteArray, settings: CryptoSettings.Symmetric): ByteArray {
        val cipher = Cipher.getInstance(settings.algorithm.javaName)
        val ips = IvParameterSpec(iv)
        cipher.init(Cipher.ENCRYPT_MODE, key, ips)
        return cipher.doFinal(plain)
    }

    suspend override fun decryptSymmetric(cipher: ByteArray, key: SecretKey, iv: ByteArray, settings: CryptoSettings.Symmetric): ByteArray {
        val cipherAlgo = Cipher.getInstance(settings.algorithm.javaName)
        val ips = IvParameterSpec(iv)
        cipherAlgo.init(Cipher.DECRYPT_MODE, key, ips)
        return cipherAlgo.doFinal(cipher)
    }

    suspend override fun deriveKeyPBKDF2WithSHA256(password: String, salt: ByteArray, iterations: Int, keyTyp: SYM_ALGO): SecretKey {
        val factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256")
        val spec = PBEKeySpec(password.toCharArray(), salt, iterations, 256)
        return factory.generateSecret(spec)
    }

    suspend override fun keyBytes(key: SecretKey): ByteArray {
        return key.encoded
    }

}