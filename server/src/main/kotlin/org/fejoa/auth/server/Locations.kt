package org.fejoa.auth.server

import org.jetbrains.ktor.locations.*


@location("/auth")
class Rpc(val rpc: String = "")