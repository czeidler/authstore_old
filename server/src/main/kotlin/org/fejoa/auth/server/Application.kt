package org.fejoa.auth.server

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.auth.JsonRPCResponse
import org.fejoa.auth.backend.Cookie
import org.fejoa.auth.backend.commands.*
import org.jetbrains.ktor.application.Application
import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.application.install
import org.jetbrains.ktor.content.TextContent
import org.jetbrains.ktor.content.defaultResource
import org.jetbrains.ktor.content.resources
import org.jetbrains.ktor.content.static
import org.jetbrains.ktor.features.*
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.locations.Locations
import org.jetbrains.ktor.logging.CallLogging
import org.jetbrains.ktor.routing.routing
import org.jetbrains.ktor.sessions.SessionCookieTransformerMessageAuthentication
import org.jetbrains.ktor.sessions.SessionCookiesSettings
import org.jetbrains.ktor.sessions.withCookieByValue
import org.jetbrains.ktor.sessions.withSessions
import org.jetbrains.ktor.transform.transform
import org.jetbrains.ktor.util.hex


fun toJson(data: Any): String {
    val mapper = ObjectMapper()
    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    return mapper.writeValueAsString(data)
}

fun Application.main() {
    install(DefaultHeaders)
    install(CallLogging)
    install(ConditionalHeaders)
    install(PartialContentSupport)
    install(Compression)
    install(Locations)
    install(StatusPages) {
        exception<NotImplementedError> { call.respond(HttpStatusCode.NotImplemented) }
    }

    routing {
        rpc(listOf(
                RegisterCommand(),
                LoginCommand(),
                StatusCommand(),
                LogoutCommand(),
                GetIndexCommand(),
                AddToIndexCommand(),
                RemoveFromIndexCommand(),
                PullCommand(),
                PushCommand()
        ))

        static {
            defaultResource("web/TestPage.html", "web")
            defaultResource("web/SubmitTest.html", "web")
            resources("web")
        }
    }

    val hashKey = hex("52365896819b196857a326945cf4")
    // WARNING: this should be called withCookies, i.e. sessions are stored in cookies!
    withSessions<Cookie> {
        withCookieByValue {
            settings = SessionCookiesSettings(
                    transformers = listOf(SessionCookieTransformerMessageAuthentication(hashKey)))
        }
    }

    transform.register<JsonRPCResponse> {
        TextContent(toJson(it), ContentType.Application.Json)
    }
}