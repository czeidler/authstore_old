package org.fejoa.auth.server

import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.auth.JsonRPCError
import org.fejoa.auth.JsonRPCResponse
import org.fejoa.auth.backend.*
import org.jetbrains.ktor.application.call
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.locations.get
import org.jetbrains.ktor.locations.post
import org.jetbrains.ktor.routing.Route


enum class RPC_ERROR(val code: Int) {
    ERROR(-1),
    INTERNAL(-2)
}


fun Route.rpc(handlerList: List<JsonHandler>) {
    post<Rpc> { loginRequest ->
        val jsonCall = ObjectMapper().readValue(loginRequest.rpc, JsonRPCCallReceiver::class.java)
        val handler = handlerList.firstOrNull { it.name == jsonCall.method} ?:
                return@post call.respond(JsonRPCResponse(jsonCall.id, null,
                        JsonRPCError(RPC_ERROR.INTERNAL.code,"Unknown method: ${jsonCall.method}")))
        try {
            val result = handler.handle(jsonCall.params, call)
            call.respond(JsonRPCResponse(jsonCall.id, result))
        } catch (e: Exception) {
            call.respond(JsonRPCResponse(jsonCall.id, null, JsonRPCError(RPC_ERROR.ERROR.code, e.message ?: "")))
        }
    }

    get<Rpc> {
        call.respond(HttpStatusCode.MethodNotAllowed)
    }
}
