package org.fejoa.auth.crypto


class CryptoHelper<SymKey>(val crypto: CryptoInterface<SymKey>) {
    suspend fun hash(data: ByteArray, algorithm: HASH_ALGO): ByteArray {
        return crypto.hash(data, parseHashAlgo(algorithm.name))
    }

    suspend fun generateId(): String {
        var result = crypto.hex(hash(generateSalt16(), HASH_ALGO.SHA256))
        if (result.length > 40)
            result = result.substring(0, 40)
        return result
    }

    fun generateSalt16(): ByteArray {
        return crypto.random(16)
    }

    suspend fun generateKey(size: Int, settings: CryptoSettings.Symmetric): SymKey {
        return crypto.secretKey(crypto.random(size), settings)
    }
}
