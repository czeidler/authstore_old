package org.fejoa.auth

// register
val REGISTER_METHOD = "register"
class RegisterArg(val user: String = "", val authKey: UserKey? = null)

/**
 * If user != "" the register process was successful
 */
class RegisterResponseArg(val user: String = "", val message: String = "")

// login
val LOGIN_METHOD = "login"
class EKE2LoginArg(val user: String = "", val protocol: String = "", val state: String = "", val gpGroup: String = "",
                   val encGY: String? = null, val iv: String? = null, val authToken: String? = null)

/**
 * @encGX encrypted gx value, stored in base64
 * @iv iv used for gx encryption, stored in base64
 */
class EKE2LoginInitResponseArg(val authKeyParams: UserKeyParams? = null, val encGX: String, val iv: String)

class EKE2LoginFinishResponseArg(val authToken: String = "")


// Status
val STATUS_METHOD = "status"
class StatusResponseArg(val currentUser: String? = null)

// Logout
val LOGOUT_METHOD = "logout"
class LogoutResponseArg(val message: String = "")


/**
 * @param name the human readable name in the index
 * @branch the name of the branch
 */
class IndexEntry(val name: String = "", val branch: String = "")

val GET_INDEX_METHOD = "getIndex"
class GetIndexResponseArg(val branches: Array<IndexEntry> = Array(0, { IndexEntry() }))

val ADD_TO_INDEX_METHOD = "addToIndex"
class AddToIndexArg(val name: String = "", val branch: String = "")

val REMOVE_FROM_INDEX_METHOD = "removeFromIndex"
class RemoveFromIndexArg(val name: String = "")

val PULL_METHOD = "pull"
class PullArg(val context: String = "", val branch: String = "")
// data is a base64 encoded byte array
class PullResponseArg(val data: String = "")

val PUSH_METHOD = "push"
// data is a base64 encoded byte array
class PushArg(val context: String = "", val branch: String = "", val data: String = "")
