package org.fejoa.auth

import org.fejoa.auth.crypto.HASH_ALGO
import org.fejoa.auth.crypto.KDF_ALGO


interface KDFParams

/**
 * @salt encoded as base64
 */
data class PDKDF2Params(val algorithm: String = "", val iterations: Int = 1, val salt: String = "") : KDFParams {
    constructor(algorithm: KDF_ALGO, iterations: Int, salt: String) : this(algorithm.name, iterations, salt)
}

/**
 * @salt encoded as base64
 */
data class UserKeyParams(var baseKeyParams: KDFParams = PDKDF2Params(), val algorithm: String = "", val salt: String = "") {
    constructor(baseKeyParams: KDFParams, algorithm: HASH_ALGO, salt: String) : this(baseKeyParams, algorithm.name, salt)
}

/**
 * @key encoded as base64
 */
class BaseKey(val key: String, val params: KDFParams) {
    companion object
}

/**
 * @key encoded as base64
 */
class UserKey(val key: String = "", val params: UserKeyParams = UserKeyParams())

class AccountSettings(val baseKeyParams: List<KDFParams>, val authUserKey: String, val keyStore: String)