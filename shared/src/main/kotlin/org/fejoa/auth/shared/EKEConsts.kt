package org.fejoa.auth.shared

class EKEConsts {
    companion object {
        val METHOD_NAME = "login"

        val FEJOA_EKE2_SHA256 = "Fejoa_EKE2_SHA256"

        val ENC_GX = "encX"
        val ENC_GY = "encY"

        val STATE_INIT = "init"
        val STATE_FINISH = "finish"

        val RFC5114_2048_256 = "RFC5114_2048_256"
    }
}