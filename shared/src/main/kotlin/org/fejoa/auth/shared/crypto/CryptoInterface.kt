package org.fejoa.auth.crypto


interface CryptoInterface<SymKey> {
    /**
     * Workaround for js <-> kotlin array mixups...
     */
    fun isEqual(array1: ByteArray, array2: ByteArray): Boolean

    fun toBase64(data: ByteArray): String
    fun fromBase64(base64: String): ByteArray

    fun hex(data: ByteArray): String

    suspend fun hash(data: ByteArray, algorithm: HASH_ALGO): ByteArray
    fun random(size: Int): ByteArray

    suspend fun secretKey(secret: ByteArray, symSettings: CryptoSettings.Symmetric): SymKey
    suspend fun encryptSymmetric(plain: ByteArray, key: SymKey, iv: ByteArray, settings: CryptoSettings.Symmetric): ByteArray
    suspend fun decryptSymmetric(cipher: ByteArray, key: SymKey, iv: ByteArray, settings: CryptoSettings.Symmetric): ByteArray

    suspend fun deriveKeyPBKDF2WithSHA256(password: String, salt: ByteArray, iterations: Int, keyTyp: SYM_ALGO): SymKey

    suspend fun keyBytes(key: SymKey): ByteArray
}