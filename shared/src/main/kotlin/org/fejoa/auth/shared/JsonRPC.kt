package org.fejoa.auth


class JsonRPCCall(val method: String = "", val params: Any? = null, val id: Int = -1) {
    var jsonrpc: String? = "2.0"
}


class JsonRPCResponse(val id: Int = -1, val result: Any? = null, val error: JsonRPCError? = null) {
    var jsonrpc: String? = "2.0"
}

class JsonRPCError(val code: Int, val message: String, val data: Any? = null)


class JsonRPCClient {
    private var callId: Int = 0

    private fun getCallId(): Int {
        callId++
        return callId
    }

    fun call(method: String, params: Any? = null): JsonRPCCall {
        return JsonRPCCall(method, params, getCallId())
    }
}