package org.fejoa.auth.crypto

enum class KDF_ALGO {
    PBKDF2WithSHA256
}

enum class HASH_ALGO(val nameJs: String) {
    SHA256("SHA-256")
}

enum class DH_GROUP {
    RFC5114_2048_256
}

fun parseHashAlgo(algorithm: String): HASH_ALGO {
    when (algorithm) {
        HASH_ALGO.SHA256.name -> return HASH_ALGO.SHA256
        else -> throw Exception("Unsupported hash algorithm: $algorithm")
    }
}

enum class SYM_ALGO(val jsName: String, val javaName: String) {
    AES_CTR("AES-CTR", "AES/CTR/NoPadding")
}

fun parseSymAlgo(algorithm: String): SYM_ALGO {
    when (algorithm) {
        SYM_ALGO.AES_CTR.javaName -> return SYM_ALGO.AES_CTR
        else -> throw Exception("Unsupported symmetric crypto algorithm: $algorithm")
    }
}

class CryptoSettings {
    class Symmetric(val algorithm: SYM_ALGO = SYM_ALGO.AES_CTR, val keyType: String = "AES", val keySize: Int = 32 * 8,
                    val ivSize: Int = 16 * 8)
}