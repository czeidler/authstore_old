importScripts('base64js.min.js');

onmessage = function(message) {
    var password = message.data.password;
    var kdfParams = message.data.kdfParams;
    if (kdfParams.algorithm == "PBKDF2WithSHA256") {
        deriveKeyPBKDF2WithSHA256(password, base64js.toByteArray(kdfParams.salt), kdfParams.iterations, "AES-CTR")
            .then(function(key) {
                self.postMessage({key: key});
            }, function(error) {
                throw error;
            });
    } else {
        throw "Unsupported kdf method";
    }
}

deriveKeyPBKDF2WithSHA256 = function(password, salt, iterations, keyTyp) {
    return crypto.subtle.importKey(
            "raw",
            new TextEncoder().encode(password),
            {"name": "PBKDF2"},
            false,
            ["deriveBits", "deriveKey"]
    ).then(function(passwordKey) {
        return crypto.subtle.deriveKey(
            {   "name": "PBKDF2",
                "salt": salt,
                "iterations": iterations,
                "hash": "SHA-256"
            },
            passwordKey,
            {   "name": keyTyp,
                "length": 256
            },
            true,
            ["encrypt", "decrypt"]
        );
    });
}