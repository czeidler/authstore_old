chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        if (request.method == "getPageInfo") {
            getPageInfo(sendResponse);
        } else if (request.method == "fillRegisterPassword") {
            fillRegisterPassword(request.password);
        } else if (request.method == "updateLoginStatus") {
            updateLoginStatus(request);
        } else {
            sendResponse({status: -1, message: "invalid request"});
        }
    });

function PortalInfo(status, message = "", path = "") {
    this.status = status;
    this.message = message;
    this.path = path;
}

function PageInfo(portalInfo, passwordFormType) {
    this.portalInfo = portalInfo;
    this.passwordFormType = passwordFormType;
}

var getPageInfo = function(sendResponse) {
    var portalInfo = getPortalInfo();
    var passwordFormType = getPasswordFormType();
    var pageInfo = new PageInfo(portalInfo, passwordFormType);
    sendResponse(pageInfo);
}

var getPortalInfo = function() {
    var fejoaAuthDiv = document.getElementById("fejoa-auth");
    if (fejoaAuthDiv == null)
        return new PortalInfo(-1, "fejoa-auth element not found");

    var path = fejoaAuthDiv.getAttribute("data-path")
    if (path == null)
        return new PortalInfo(-1, "data-path element not set");

    return new PortalInfo(0, "", path);
}

var getPasswordFormType = function() {
    for (var i = 0; i < document.forms.length; i++) {
        var form = document.forms[i];
        var fields = PasswordFields.getPasswordFields(form, false);
        if (!fields) {
            return "none";
        } else if (fields.length == 1) {
            return "login";
        } else if (fields.length == 2) {
            return "register";
        } else if (fields.length == 3) {
            return "new_password";
        }
    }
    return "none";
}

var fillRegisterPassword = function(password) {
    for (var i = 0; i < document.forms.length; i++) {
        var form = document.forms[i];
        var fields = PasswordFields.getPasswordFields(form, false);
        if (!fields || fields.length < 2)
            return;
        // Fill the last to password fields. This assumes that if there are 3 field it is a "change password" page
        // and the first password field is the old password
        fields[fields.length - 1].element.value = password;
        fields[fields.length - 2].element.value = password;
    }
}

var updateLoginStatus = function(request) {
    var user = request.user;
    var authStatus = request.authStatus;
    window.postMessage({
            type: "fejoa_auth_update",
            user: user,
            authStatus: authStatus
        }, "*");
}

// init password manager functionality
var initPasswordManager = function() {
    for (var i = 0; i < document.forms.length; i++) {
        document.forms[i].addEventListener("submit", function() {
            var form = this;
            var [usernameField, newPasswordField, oldPasswordField] = PasswordFields.getFormFields(form, true);
            if (usernameField != null && newPasswordField != null) {
                // store credentials
                chrome.runtime.sendMessage({
                    method: "storeCredentials",
                    user: usernameField.value,
                    password: newPasswordField.value
                });
            }
        });

        var [usernameField, newPasswordField, oldPasswordField] = PasswordFields.getFormFields(document.forms[i],
            false);
        if (usernameField != null && newPasswordField != null && oldPasswordField == null) {
            // listen for username input
            usernameField.addEventListener('input', function () {
                // request credentials from the password manager and fill them in
                chrome.runtime.sendMessage({
                        method: "getCredentials",
                        user: usernameField.value
                    }, function(response) {
                    var user = response.user
                    var password = response.password;
                    if (password != "" && user == usernameField.value)
                        newPasswordField.value = password;
                });
            });
        }
    }
}

initPasswordManager()