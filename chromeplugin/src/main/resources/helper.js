function hex(buffer) { // buffer is an ArrayBuffer
  return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
}


// from crypto-js:

// Convert a hex string to a byte array
function hexToBytes(hex) {
    // Clemens: if the string length is uneven start with a single char
    var c = 0;
    var bytes = [];
    if (hex.length % 2 != 0) {
        bytes.push(parseInt(hex.substr(c, 1), 16));
        c += 1;
    }
    for (; c < hex.length; c += 2)
    bytes.push(parseInt(hex.substr(c, 2), 16));
    return bytes;
}

// Convert a byte array to a hex string
function bytesToHex(bytes) {
    for (var hex = [], i = 0; i < bytes.length; i++) {
        hex.push((bytes[i] >>> 4).toString(16));
        hex.push((bytes[i] & 0xF).toString(16));
    }
    return hex.join("");
}