package org.fejoa.auth

import org.fejoa.auth.crypto.*
import org.fejoa.auth.crypto.AuthProtocolEKE2_SHA3_256_CTR
import org.fejoa.auth.js.await
import org.fejoa.auth.js.launch

class Test {
    suspend fun doTests() {
        testPasswordEnvelope()
        testCryptoContainer()
        testKDFWorker()
        testEKE2()
        testKDFAndEncryption()
        testGenerateRandom()
    }

    suspend private fun testPasswordEnvelope() {
        test("testPasswordEnvelope")
        val keyCache = BaseKeyCache()
        val userKey = keyCache.generateBaseKey("test", KDF_ALGO.PBKDF2WithSHA256, 100)
                .await().deriveUserKey(HASH_ALGO.SHA256)
        val envelope = PasswordProtectedEnvelope().pack(userKey, TextEncoder().encode("data"))

        val plain = PasswordProtectedEnvelope().unpack("test", keyCache, envelope).await()
        console.log("expected: 'data', got: ${TextDecoder().decode(plain.first)}")
    }

    suspend private fun testCryptoContainer() {
        test("testCryptoContainer")

        val container = Container.create()

        val cipher = container.encrypt(TextEncoder().encode("data"))
        val plain = Container(container.key, container.iv, container.settings).decrypt(cipher)
        console.log("expected: 'data', got: ${TextDecoder().decode(plain)}")
    }

    private fun testKDFWorker() {
        test("testKDFWorker")
        launch {
            val salt = base64js.encode(CryptoHelper(CryptoJs).generateSalt16())
            val kdfParams = PDKDF2Params(KDF_ALGO.PBKDF2WithSHA256.name, 100, salt)
            try {
                val worker = KDFWorker("test", kdfParams)
                val kdfWorker = worker.get()
                var key = kdfWorker.await()
                console.log("Worker generated key: " + CryptoJs.toBase64(CryptoJs.keyBytes(key)))
            } catch(e: Exception) {
                console.log("KDFWorker: " + e.message)
            }
        }
    }

    private fun debug(message: String) {
        console.log(message)
    }

    private fun test(name: String) {
        console.log("TEST: $name")
    }

    private fun testGenerateRandom() {
        test("testGenerateRandom")
        val random = CryptoJs.random(32)
        debug("Generated: " + CryptoJs.hex(random))
    }

    suspend private fun testKDFAndEncryption() {
        test("testKDFAndEncryption")
        val salt = CryptoHelper(CryptoJs).generateSalt16()
        val key = CryptoJs.deriveKeyPBKDF2WithSHA256("password", salt, 2, SYM_ALGO.AES_CTR)
        debug("Derived key: " + CryptoJs.hex(CryptoJs.keyBytes(key)))

        val plain = "Plain text"
        val iv = CryptoHelper(CryptoJs).generateSalt16()
        val settings = CryptoSettings.Symmetric(
                algorithm = SYM_ALGO.AES_CTR,
                keyType = "AES",
                keySize = 256,
                ivSize = 16 * 8)
        val cipher = CryptoJs.encryptSymmetric(TextEncoder().encode(plain), key, iv, settings)
        debug("cipher: " + CryptoJs.hex(cipher))
        val decrypted = TextDecoder().decode(CryptoJs.decryptSymmetric(cipher, key, iv, settings))
        debug("pain: " + decrypted)
        if (plain != decrypted)
            throw Exception("decryption failed")
    }

    suspend private fun testEKE2() {
        test("testEKE2")

        val secretKey = CryptoJs.deriveKeyPBKDF2WithSHA256("Password", CryptoHelper(CryptoJs).generateSalt16(), 10, SYM_ALGO.AES_CTR)
        val secret = CryptoJs.keyBytes(secretKey)

        val server = AuthProtocolEKE2_SHA3_256_CTR.createProver(CryptoJs, DH_GROUP.RFC5114_2048_256.name, secret)
        val encGx = server.getEncGX()
        val client = AuthProtocolEKE2_SHA3_256_CTR.createVerifier(CryptoJs, DH_GROUP.RFC5114_2048_256.name, secret,
                encGx.first, encGx.second)
        val encGy = client.getEncGy()
        val serverFinish = server.setVerifierResponse(encGy.first, encGy.second, client.getAuthToken()) ?: throw Exception("Verifier denied access")
        if (client.verify(serverFinish.getAuthToken()))
            console.log("EKE2 succeeded")
    }
}