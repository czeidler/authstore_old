package org.fejoa.auth

import org.fejoa.auth.js.await
import org.fejoa.auth.js.chrome
import org.w3c.dom.url.URLSearchParams
import org.w3c.fetch.*
import kotlin.js.json

/**
 * For some reason the promise exception doesn't get propagated. As a workaround return null on a network failure.
 */
suspend fun postRequest(url: String, body: dynamic): Response? {
    return chrome.extension.getBackgroundPage().fetch(url, object : RequestInit {
        override var method: String? = "POST"
        override var body: dynamic = body
        override var credentials: RequestCredentials? = RequestCredentials.INCLUDE
        override var headers: dynamic = json("Accept" to "application/json")
    }).then({it},{null}).await()
}

suspend fun postRPC(url: String, call: JsonRPCCall): Any {
    val response = postRequest(url, URLSearchParams().apply {
        append("rpc", JSON.stringify(call))
    }) ?: throw Exception("Connection error to: $url")
    if (!response.ok)
        throw Exception("Connection error: ${response.statusText}")
    val text = response.text().await()
    //console.log("postRPC received: $text")
    val jsonRpcResponse = JSON.parse<JsonRPCResponse>(text)
    if (jsonRpcResponse.error != null)
        throw Exception(jsonRpcResponse.error.message)
    if (jsonRpcResponse.result == null)
        throw Exception("Missing JSON RPC result parameter")
    return jsonRpcResponse.result
}