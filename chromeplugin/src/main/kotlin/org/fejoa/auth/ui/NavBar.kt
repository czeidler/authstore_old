package org.fejoa.auth.ui

import org.w3c.dom.HTMLButtonElement
import org.w3c.dom.HTMLDivElement
import org.w3c.dom.HTMLSpanElement
import org.w3c.dom.Window
import kotlin.dom.addClass
import kotlin.dom.removeClass
import kotlin.properties.Delegates


class NavBar {
    enum class VIEW {
        AUTH,
        PASSWORD_MANAGER
    }
    var view: VIEW by Delegates.observable(VIEW.AUTH) { _, _, _ ->
        onViewChanged?.invoke()
    }

    var onViewChanged: (() -> Unit)? = null
}

class NavBarController(val window: Window, val navBar: NavBar, val pageInfo: PageInfoModel) {
    val document = window.document
    val titleSpan = document.getElementById("nav-bar-title").unsafeCast<HTMLSpanElement>()
    val authButton = document.getElementById("nav-auth-button").unsafeCast<HTMLButtonElement>()
    val pwButton = document.getElementById("nav-pm-button").unsafeCast<HTMLButtonElement>()
    val authDiv = document.getElementById("fejoa-auth-container").unsafeCast<HTMLDivElement>()
    val pdDiv = document.getElementById("password-manager-container").unsafeCast<HTMLDivElement>()

    init {
        navBar.onViewChanged = this::onViewChanged
        pageInfo.onPageInfoChanged.add {
            if (pageInfo.hasFejoaAuth()) {
                authButton.disabled = false
                authButton.setAttribute("title", "")
            } else {
                authButton.disabled = true
                authButton.setAttribute("title", "This page does not support Fejoa Auth")
            }
        }

        authButton.addEventListener("click", {
            navBar.view = NavBar.VIEW.AUTH
        })
        pwButton.addEventListener("click", {
            navBar.view = NavBar.VIEW.PASSWORD_MANAGER
        })

        // trigger initial update
        onViewChanged()
    }

    private fun onViewChanged() {
        when (navBar.view) {
            NavBar.VIEW.AUTH -> {
                titleSpan.innerText = "Authentication"
                pwButton.removeClass("hidden")
                authButton.addClass("hidden")

                pdDiv.addClass("hidden")
                authDiv.removeClass("hidden")
            }
            NavBar.VIEW.PASSWORD_MANAGER -> {
                titleSpan.innerText = "Password Manager"
                pwButton.addClass("hidden")
                authButton.removeClass("hidden")

                pdDiv.removeClass("hidden")
                authDiv.addClass("hidden")
            }
        }
    }
}