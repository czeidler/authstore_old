package org.fejoa.auth.ui

import org.fejoa.auth.Context
import org.fejoa.auth.FejoaAuth
import org.fejoa.auth.FejoaAuthProtocol
import org.fejoa.auth.Remote
import org.fejoa.auth.crypto.TextEncoder
import org.fejoa.auth.database.EncryptedDatabase
import org.fejoa.auth.database.JsonBackedDatabase
import org.fejoa.auth.database.PasswordProtectedDatabase
import org.fejoa.auth.js.launch
import org.fejoa.auth.passwordmanager.PasswordManagerAccount
import org.w3c.dom.*
import kotlin.dom.addClass


class RemoteController(pwController: PasswordManagerController, val context: Context) {
    val window = pwController.window
    val document = window.document
    val fejoaAuth = context.fejoaAuth
    val accountManager = pwController.accountManager

    var pmAccount: PasswordManagerAccount? = null
    set(value) {
        pmAccount?.onRemotesChangedListeners?.remove(this::onRemoteChanged)
        value?.onRemotesChangedListeners?.add(this::onRemoteChanged)
        field = value
        onRemoteChanged()
    }

    val addRemoteDropdown = document.getElementById("pm-remote-add-dropdown").unsafeCast<HTMLUListElement>()
    val remoteList = document.getElementById("pm-remote-list").unsafeCast<HTMLUListElement>()

    val statusListener = FejoaAuth.StatusListener("*", {
        onStatusUpdated()
    })

    init {
        fejoaAuth.statusListeners += statusListener
        window.addEventListener("beforeunload", {
            fejoaAuth.statusListeners.remove(statusListener)
        })

        onStatusUpdated()
        onRemoteChanged()
    }

    private fun onRemoteChanged() {
        // repopulate the remote list
        while (remoteList.firstChild != null)
            remoteList.removeChild(remoteList.firstChild!!)

        val account = pmAccount ?: return
        launch {
            account.listRemotes().forEach { remote ->
                val li = document.createElement("li").unsafeCast<HTMLLIElement>()
                li.addClass("list-group-item")
                val name = document.createElement("span").unsafeCast<HTMLSpanElement>()
                name.innerText = "${remote.user}@${remote.url}"

                val pushButton = document.createElement("button").unsafeCast<HTMLButtonElement>()
                pushButton.innerText = "Push"
                pushButton.addClass("btn")
                pushButton.addClass("pull-right")
                pushButton.addEventListener("click", { launch { push(account, remote) } })

                val pullButton = document.createElement("button").unsafeCast<HTMLButtonElement>()
                pullButton.innerText = "Pull"
                pullButton.addClass("btn")
                pullButton.addClass("pull-right")

                li.appendChild(name)
                li.appendChild(pushButton)
                li.appendChild(pullButton)

                remoteList.appendChild(li)
            }
        }
    }

    private fun onStatusUpdated() {
        // repopulate the drop down
        while (addRemoteDropdown.firstChild != null)
            addRemoteDropdown.removeChild(addRemoteDropdown.firstChild!!)
        // fill logged in remotes
        fejoaAuth.getAutStatusList().filter { it.second.status == FejoaAuth.Status.LOGGED_IN }.forEach {
            val origin = it.first
            val li = document.createElement("li")
            li.addEventListener("click", {
                launch {
                    onAddRemote(origin)
                }
            })
            val ref = document.createElement("a").unsafeCast<HTMLLinkElement>()
            ref.href = "#"
            ref.innerText = "${it.second.user}@${it.second.url}"
            li.appendChild(ref)
            addRemoteDropdown.appendChild(li)
        }
    }

    suspend fun onAddRemote(origin: String) {
        val account = pmAccount ?: return
        val authStatus = fejoaAuth.getAuthStatus(origin) ?: return
        val user = authStatus.user ?: return
        val url = authStatus.url ?: return
        account.addRemote(Remote.create(user, url))
        account.commit()
    }

    suspend private fun push(account: PasswordManagerAccount, remote: Remote) {
        val protocol = FejoaAuthProtocol(context.jsonRPC, context.keyCache)

        // push the wallets
        account.wallets.forEach {
            val jsonStorageDB = it.storageDir.database as EncryptedDatabase
            protocol.push(remote.url, account.name, it.getId(),
                    TextEncoder().encode(JSON.stringify(jsonStorageDB.toEnvelope())))
        }
        // push the main account
        val passwordDatabase = account.storageDir.database as PasswordProtectedDatabase
        protocol.push(remote.url, account.name, account.getId(),
                TextEncoder().encode(JSON.stringify(passwordDatabase.toEnvelope())))

        // update the index
        protocol.addToIndex(remote.url, account.name, account.getId())
    }
}