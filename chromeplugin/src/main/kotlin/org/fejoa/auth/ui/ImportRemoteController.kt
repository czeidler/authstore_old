package org.fejoa.auth.ui

import org.fejoa.auth.*
import org.fejoa.auth.crypto.TextDecoder
import org.fejoa.auth.js.await
import org.fejoa.auth.js.launch
import org.fejoa.auth.passwordmanager.PMAccountManager
import org.w3c.dom.*
import kotlin.dom.addClass


class ImportRemoteModel(val accountManager: PMAccountManager, val fejoaAuth: FejoaAuth,
                        val protocol: FejoaAuthProtocol) {
    var availableRemotes: List<Remote> = ArrayList()
    var remoteIndexEntries: Array<IndexEntry> = emptyArray()

    var selectedRemote: Remote? = null
    var waitingForContext = false
    var selectedIndexEntry: IndexEntry? = null
    var selectedIndexEntryIsValid = false
    var selectedIndexEntryInvalidError = ""

    var lastError: String? = null

    val listeners = ArrayList<() -> Unit>()

    private fun notifyUpdate() {
        listeners.forEach { it.invoke() }
    }

    private val statusListener = FejoaAuth.StatusListener("*", {
        onStatusUpdated()
    })

    init {
        fejoaAuth.statusListeners += statusListener
    }

    fun shutdown() {
        fejoaAuth.statusListeners.remove(statusListener)
    }

    fun import() {
        lastError = ""
        val remote = selectedRemote ?: return
        val indexEntry = selectedIndexEntry ?: return

        launch {
            try {
                accountManager.import(protocol, remote, indexEntry)
            } catch(e: dynamic) {
                lastError = e.message ?: e
            }
            notifyUpdate()
        }
    }

    fun selectRemote(remote: Remote?) {
        lastError = ""
        selectedRemote = remote
        selectedRemote?.let {
            requestContextList(it)
        }
        notifyUpdate()
    }

    fun selectContext(entry: IndexEntry) {
        lastError = ""
        selectedIndexEntry = entry

        selectedIndexEntryIsValid = false
        val hasAccountWithSameName = accountManager.getAccounts()
                .firstOrNull { it.getAccountName() == entry.name } != null
        val hasAccount = accountManager.getAccounts()
                .map { it.account }.filterNotNull()
                .firstOrNull { it.getId() == entry.branch } != null
        if (!hasAccount && !hasAccountWithSameName) {
            selectedIndexEntryIsValid = true
        } else if (hasAccount) {
            selectedIndexEntryInvalidError = "exist locally"
        } else if (hasAccountWithSameName) {
            selectedIndexEntryInvalidError = "local account with same name exists"
        }

        notifyUpdate()
    }

    private fun requestContextList(remote: Remote) {
        val url = remote.url
        launch {
            try {
                waitingForContext = true
                remoteIndexEntries = protocol.getIndex(url)
                waitingForContext = false
                notifyUpdate()
            } catch(e: dynamic) {
                lastError = e.message ?: e
                notifyUpdate()
            }
        }
    }

    private fun onStatusUpdated() {
        availableRemotes = fejoaAuth.getAutStatusList().map { it.second }.filter {
            it.status == FejoaAuth.Status.LOGGED_IN
        }.map {
            Remote("-1", it.user ?: "null", it.url ?: "null")
        }
        selectedRemote?.let { selected ->
            if (availableRemotes.find { it.url == selected.url && it.user == selected.user } == null)
                selectRemote(null)
        }
        notifyUpdate()
    }
}

class ImportRemoteController(val pwController: PasswordManagerController, val context: Context,
                             val log: LogController) {
    val window = pwController.window
    val document = window.document
    val importModel = ImportRemoteModel(context.accountManager, context.fejoaAuth, FejoaAuthProtocol(context.jsonRPC,
            context.keyCache))

    val importContainer = document.getElementById("pm-import-account-container").unsafeCast<HTMLDivElement>()
    val remotesSelect = document.getElementById("pm-import-available-remotes").unsafeCast<HTMLSelectElement>()
    val contextSelect = document.getElementById("pm-import-available-context").unsafeCast<HTMLSelectElement>()
    val importButton = document.getElementById("pm-import-button").unsafeCast<HTMLButtonElement>()

    // select option value -> remote
    var optionToRemoteMap = HashMap<String, Remote>()
    // select option value -> IndexEntry
    var optionToIndexEntryMap = HashMap<String, IndexEntry>()

    init {
        importModel.listeners += { onUpdate() }

        window.addEventListener("beforeunload", {
            importModel.shutdown()
        })

        remotesSelect.addEventListener("change", {
            optionToRemoteMap[remotesSelect.value]?.let {
                importModel.selectRemote(it)
            }
        })

        contextSelect.addEventListener("change", {
            optionToIndexEntryMap[contextSelect.value]?.let {
                importModel.selectContext(it)
            }
        })

        importButton.addEventListener("click", {
            importModel.import()
        })

        pwController.addTabContentContainer(importContainer)

        onUpdate()
    }

    fun populateImportMenu(dropDownMenu: HTMLUListElement) {
        val separator = document.createElement("li")
        separator.addClass("divider")
        dropDownMenu.appendChild(separator)
        // import from remote
        val importItem = document.createElement("li")
        importItem.addEventListener("click", {
            pwController.errorMessage = ""
            onImportSelected()
        })
        val link = document.createElement("a").unsafeCast<HTMLLinkElement>()
        link.innerText = "Import from remote"
        link.href = "#pm-account-view-tab"
        link.setAttribute("data-toggle", "tab")
        importItem.appendChild(link)
        dropDownMenu.appendChild(importItem)
    }

    private fun onImportSelected() {
        pwController.accountStatusBar.hidden = true
        pwController.showContainer(importContainer)
    }

    private fun onUpdate() {
        log.error(importModel.lastError ?: "")

        // repopulate remote combo box
        while (remotesSelect.firstChild != null)
            remotesSelect.removeChild(remotesSelect.firstChild!!)

        // TODO kotlin problem clear doesn't work...
        optionToRemoteMap = HashMap()
        // fill logged in remotes
        importModel.availableRemotes.forEachIndexed { index, remote ->
            val option = document.createElement("option").unsafeCast<HTMLOptionElement>()
            val optionValue = "$index"
            option.value = optionValue
            if (importModel.selectedRemote == remote)
                option.selected = true

            val ref = document.createElement("a").unsafeCast<HTMLLinkElement>()
            ref.href = "#"
            ref.innerText = "${remote.user}@${remote.url}"
            option.appendChild(ref)
            optionToRemoteMap[optionValue] = remote
            remotesSelect.appendChild(option)
        }
        // auto select the first item
        if (importModel.selectedRemote == null && importModel.availableRemotes.size > 0) {
            importModel.selectRemote(importModel.availableRemotes[0])
        }

        // TODO kotlin problem clear doesn't work...
        optionToIndexEntryMap = HashMap()
        // repopulate context combo box
        while (contextSelect.firstChild != null)
            contextSelect.removeChild(contextSelect.firstChild!!)
        contextSelect.disabled = importModel.selectedRemote == null
        importModel.remoteIndexEntries.forEachIndexed { index, entry ->
            val option = document.createElement("option").unsafeCast<HTMLOptionElement>()
            val optionValue = "$index"
            option.value = optionValue
            if (importModel.selectedIndexEntry == entry)
                option.selected = true

            val ref = document.createElement("a").unsafeCast<HTMLLinkElement>()
            ref.href = "#"
            ref.innerText = entry.name
            if (!importModel.selectedIndexEntryIsValid) {
                ref.innerText = entry.name + " (${importModel.selectedIndexEntryInvalidError})"
                option.disabled = true
            }
            option.appendChild(ref)
            optionToIndexEntryMap[optionValue] = entry
            contextSelect.appendChild(option)
        }
        // auto select the first item
        if (importModel.selectedIndexEntry == null && importModel.remoteIndexEntries.size > 0) {
            importModel.selectContext(importModel.remoteIndexEntries[0])
        }

        importButton.disabled = !importModel.selectedIndexEntryIsValid
    }
}