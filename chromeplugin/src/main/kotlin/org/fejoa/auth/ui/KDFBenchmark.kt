package org.fejoa.auth.ui

import org.fejoa.auth.BaseKey
import org.fejoa.auth.CancelablePromise
import org.fejoa.auth.KDFParams
import org.fejoa.auth.PDKDF2Params
import org.fejoa.auth.crypto.*
import org.fejoa.auth.js.await
import org.fejoa.auth.js.launch
import org.w3c.dom.HTMLButtonElement
import org.w3c.dom.HTMLSpanElement
import org.w3c.dom.Window
import kotlin.js.Date

class KDFBenchmark(val keyCache: BaseKeyCache) {
    class Result(val time: Double? = null, val nIterations: Int = 0, val promise: CancelablePromise<BaseKey>? = null)

    var result = Result()
        private set

    val listeners = ArrayList<() -> Unit>()

    private fun updateResult(newResult: Result) {
        result = newResult
        listeners.forEach { it.invoke() }
    }

    fun benchmark(kdfAlgo: KDF_ALGO, nIterations: Int) {
        val params: KDFParams
        val salt = base64js.encode(CryptoHelper(CryptoJs).generateSalt16())
        when (kdfAlgo) {
            KDF_ALGO.PBKDF2WithSHA256 -> {
                params = PDKDF2Params(KDF_ALGO.PBKDF2WithSHA256, nIterations, salt)
            }
        }
        launch {
            val startTime = Date().getTime()
            val result = keyCache.getBaseKey("password", params)
            updateResult(Result(promise = result))
            try {
                result.await()
                updateResult(Result(time = Date().getTime() - startTime, nIterations = nIterations))
            } catch (e: dynamic) {
                // canceled
                updateResult(Result())
            }
        }
    }
}

class KDFBenchmarkController(val window: Window, val benchmark: KDFBenchmark, val kdfConfig: KDFConfigController,
                             uiElementPrefix: String = "") {
    val document = window.document

    val resultView = document.getElementById("${uiElementPrefix}benchmark-result").unsafeCast<HTMLSpanElement>()
    val benchmarkButton = document.getElementById("${uiElementPrefix}benchmark-button").unsafeCast<HTMLButtonElement>()
    val cancelButton = document.getElementById("${uiElementPrefix}benchmark-cancel-button").unsafeCast<HTMLButtonElement>()

    init {
        benchmark.listeners.add(this::onResultUpdated)

        benchmarkButton.addEventListener("click", {
            val algo = kdfConfig.getKdfAlgo()
            val nIterations = kdfConfig.getNIterations()

            benchmark.benchmark(algo, nIterations)
        })

        cancelButton.addEventListener("click", {
            benchmark.result.promise?.cancel()
        })

        onResultUpdated()
    }

    fun onResultUpdated() {
        val result = benchmark.result
        if (result.promise != null) {
            // running
            resultView.innerText = "calculating..."
            benchmarkButton.disabled = true
            cancelButton.disabled = false
        } else {
            benchmarkButton.disabled = false
            cancelButton.disabled = true

            if (result.time != null)
                resultView.innerText = "${result.time} ms for ${result.nIterations} iterations"
            else
                resultView.innerText = "-"
        }
    }
}