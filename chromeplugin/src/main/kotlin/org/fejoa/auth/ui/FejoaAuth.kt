package org.fejoa.auth.ui

import org.fejoa.auth.FejoaAuth
import org.fejoa.auth.FejoaAuthPageInfo
import org.fejoa.auth.js.launch
import org.w3c.dom.*


class FejoaAuthController(val window: Window, val fejoaAuth: FejoaAuth, val pageInfo: FejoaAuthPageInfo,
                          val log: LogController) {
    val document = window.document
    val kdfConfigController = KDFConfigController(window)

    val authUrl = pageInfo.getFejoaAuthPath()

    val loginStatus = document.getElementById("login-status").unsafeCast<HTMLDivElement>()

    val currentPageLabel = document.getElementById("current-page").unsafeCast<HTMLSpanElement>()
    val loggedInUser = document.getElementById("logged-in-user").unsafeCast<HTMLSpanElement>()
    val loginRegister = document.getElementById("login-register").unsafeCast<HTMLDivElement>()
    val loginUsernameInput = document.getElementById("login-username").unsafeCast<HTMLInputElement>()
    val loginPasswordInput = document.getElementById("login-password").unsafeCast<HTMLInputElement>()

    val registerUsernameInput = document.getElementById("register-username").unsafeCast<HTMLInputElement>()
    val registerPasswordInput = document.getElementById("register-password").unsafeCast<HTMLInputElement>()
    val registerPassword2Input = document.getElementById("register-password2").unsafeCast<HTMLInputElement>()

    val logoutButton = document.getElementById("logout-button").unsafeCast<HTMLButtonElement>()
    val loginButton = document.getElementById("login-button").unsafeCast<HTMLButtonElement>()
    val registerButton = document.getElementById("register-button").unsafeCast<HTMLButtonElement>()

    val busyContainer = document.getElementById("fa-busy-container").unsafeCast<HTMLDivElement>()
    val busyStatus = document.getElementById("fa-busy-status").unsafeCast<HTMLSpanElement>()
    val cancelButton = document.getElementById("fa-cancel-button").unsafeCast<HTMLButtonElement>()

    val statusListener = FejoaAuth.StatusListener(pageInfo.origin(), {
        onAuthStatusChanged()
    })

    init {
        KDFBenchmarkController(window, KDFBenchmark(fejoaAuth.keyCache), kdfConfigController)

        fejoaAuth.statusListeners += statusListener
        window.addEventListener("beforeunload", {
            fejoaAuth.statusListeners.remove(statusListener)
        })

        currentPageLabel.innerText = pageInfo.url.hostname

        logoutButton.addEventListener("click", {
            launch {
                fejoaAuth.logOut(authUrl)
            }
        })
        loginButton.addEventListener("click", {

            enableLoginRegister(false)
            val password = loginPasswordInput.value
            loginPasswordInput.value = ""
            fejoaAuth.launchLogin(authUrl, loginUsernameInput.value, password)
        })
        registerButton.addEventListener("click", {
            enableLoginRegister(false)
            val password = registerPasswordInput.value
            val algo = kdfConfigController.getKdfAlgo()
            val nIterations = kdfConfigController.getNIterations()
            registerPasswordInput.value = ""
            registerPassword2Input.value = ""
            fejoaAuth.launchRegister(algo, authUrl, registerUsernameInput.value, password, nIterations)
        })
        cancelButton.addEventListener("click", {
            val origin = pageInfo.origin()
            val authStatus = fejoaAuth.getAuthStatus(origin) ?: return@addEventListener
            authStatus.promise?.cancel()
        })

        loginUsernameInput.oninput = {_ -> validateLoginInfo()}
        loginPasswordInput.oninput = {_ -> validateLoginInfo()}

        registerUsernameInput.oninput = {_ -> validateRegisterInfo()}
        registerPasswordInput.oninput = {_ -> validateRegisterInfo()}
        registerPassword2Input.oninput = {_ -> validateRegisterInfo()}

        // init the UI
        onAuthStatusChanged()
    }

    suspend fun start() {
        fejoaAuth.updateStatus(authUrl)
    }

    private fun validateLoginInfo() {
        val user = loginUsernameInput.value
        val password = loginPasswordInput.value
        loginButton.disabled = (user == "" || password == "")
    }
    private fun validateRegisterInfo() {
        val user = registerUsernameInput.value
        val password1 = registerPasswordInput.value
        val password2 = registerPassword2Input.value
        registerButton.disabled = (user == "" || password1 == "" || password1 != password2)
    }

    private fun enableLoginRegister(enable: Boolean) {
        loginUsernameInput.disabled = !enable
        loginPasswordInput.disabled = !enable

        registerUsernameInput.disabled = !enable
        registerPasswordInput.disabled = !enable
        registerPassword2Input.disabled = !enable

        validateLoginInfo()
        validateRegisterInfo()
    }

    private fun onAuthStatusChanged() {
        val authStatus = fejoaAuth.getAuthStatus(pageInfo.origin()) ?: return
        val user = authStatus.user

        if (authStatus.status == FejoaAuth.Status.LOGGING_IN || authStatus.status == FejoaAuth.Status.REGISTERING) {
            busyContainer.hidden = false
            loginStatus.hidden = true
            logoutButton.disabled = true
            loginRegister.hidden = true
            if (authStatus.status == FejoaAuth.Status.LOGGING_IN)
                busyStatus.innerText = "logging in as ${authStatus.user}..."
            else
                busyStatus.innerText = "registering as ${authStatus.user}..."
        } else if (user != null && user != "") {
            busyContainer.hidden = true
            loginStatus.hidden = false
            logoutButton.disabled = false
            loginRegister.hidden = true
            loggedInUser.innerText = user
            loginUsernameInput.value = ""
            registerUsernameInput.value = ""
        } else {
            busyContainer.hidden = true
            loginStatus.hidden = true
            logoutButton.disabled = true
            loginRegister.hidden = false
            enableLoginRegister(true)
        }

        onErrorOrStatusChanged(authStatus)
    }

    private fun onErrorOrStatusChanged(authStatus: FejoaAuth.AuthStatus) {
        val status = authStatus.statusMessage ?: ""
        val error = authStatus.errorMessage ?: ""
        log.status(status)
        log.error(error)
    }
}