package org.fejoa.auth.ui

import org.fejoa.auth.*
import org.fejoa.auth.js.await
import org.fejoa.auth.js.launch
import org.w3c.dom.*
import kotlin.properties.Delegates


class PageInfoModel {
    var pageInfo: Pair<FejoaAuthPageInfo?, String?> by Delegates.observable<Pair<FejoaAuthPageInfo?, String?>>(
            null to "Page info not updated") { _, _, _ ->
        onPageInfoChanged.forEach { it.invoke() }
    }

    var onPageInfoChanged = ArrayList<(() -> Unit)>()

    fun hasFejoaAuth(): Boolean {
        return pageInfo.first != null
    }

    suspend fun updatePageInfo() {
        pageInfo = FejoaAuthPageInfo.getCurrentPage().await()
    }
}


/**
 * Start the main popup and checks if the page supports fejoa auth
 */
class PopupController(val context: Context, val window: Window) {
    val pageInfo = PageInfoModel()
    val document = window.document
    val log = LogController(window)

    val navBar = NavBar()

    private fun onPageInfoChanged() {
        pageInfo.pageInfo.first?.let {
            navBar.view = NavBar.VIEW.AUTH
            launch {
                FejoaAuthController(window, context.fejoaAuth, it, log).start()
            }
        }
        pageInfo.pageInfo.second?.let {
            navBar.view = NavBar.VIEW.PASSWORD_MANAGER
        }
    }

    fun start() {
        // connect test button
        val testButton = document.getElementById("test-button").unsafeCast<HTMLButtonElement>()
        testButton.addEventListener("click", { _ ->
            launch {
                Test().doTests()
            }
        })

        NavBarController(window, navBar, pageInfo)
        pageInfo.onPageInfoChanged.add(this::onPageInfoChanged)
        launch {
            pageInfo.updatePageInfo()
        }

        // password manager
        PasswordManagerController(window, context, pageInfo, log)
    }
}
