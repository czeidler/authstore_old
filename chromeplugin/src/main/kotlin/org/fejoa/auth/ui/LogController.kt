package org.fejoa.auth.ui

import org.w3c.dom.HTMLParagraphElement
import org.w3c.dom.Window


class LogController(val window: Window) {
    val document = window.document

    val statusLabel = document.getElementById("status-message").unsafeCast<HTMLParagraphElement>()
    val errorLabel = document.getElementById("error-message").unsafeCast<HTMLParagraphElement>()

    fun status(message: String) {
        statusLabel.hidden = message == ""
        statusLabel.innerText = message
    }

    fun error(message: String) {
        errorLabel.hidden = message == ""
        errorLabel.innerText = message
    }


}