package org.fejoa.auth.ui

import org.fejoa.auth.crypto.KDF_ALGO
import org.w3c.dom.HTMLInputElement
import org.w3c.dom.HTMLSelectElement
import org.w3c.dom.Window


class KDFConfigController(val window: Window, uiElementPrefix : String = "") {
    val document = window.document

    val kdfAlgo = document.getElementById("${uiElementPrefix}kdf-algo").unsafeCast<HTMLSelectElement>()
    val nIterationsInput = document.getElementById("${uiElementPrefix}n-iterations").unsafeCast<HTMLInputElement>()

    fun getKdfAlgo(): KDF_ALGO {
        val value = kdfAlgo.value
        when (value) {
            "PBKDF2" -> return KDF_ALGO.PBKDF2WithSHA256
            else -> throw Exception("Unexpected KDF algorithm: $value")
        }
    }

    fun getNIterations(): Int {
        return nIterationsInput.valueAsNumber.toInt().let {
            if (it <= 0) 1 else it
        }
    }
}