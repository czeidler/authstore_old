package org.fejoa.auth

import org.fejoa.auth.ui.PopupController
import org.w3c.dom.Window
import kotlin.browser.window


/**
 * Helper to start the popup code from the popup.js
 */
class FejoaAuthModule(val context: Context) {
    fun showPopup(window: Window) {
        PopupController(context, window).start()
    }
}


fun main(args: Array<String>) {
    /**
     *  Workaround to make background functionality available to the popup
     *
     *  The problem is that the compiled Kotlin bundle has no public methods. By storing the fejoaAuthModule in the
     *  background window it can be access from the popup.
     */
    val dynamicWindow: dynamic = window
    val context = Context()
    dynamicWindow.fejoaAuthModule = FejoaAuthModule(context)
}