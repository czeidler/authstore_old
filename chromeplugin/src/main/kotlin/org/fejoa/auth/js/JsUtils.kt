package org.fejoa.auth.js

import org.khronos.webgl.ArrayBuffer
import org.khronos.webgl.Uint8Array


fun concat(vararg buffers: ArrayBuffer): Uint8Array {
    val totalSize = buffers.map { it.byteLength }.sum()
    var out = Uint8Array(totalSize)
    var position = 0
    for (buffer in buffers) {
        out.set(Uint8Array(buffer), position)
        position += buffer.byteLength
    }
    return out
}


external object Object {
    fun keys(obj: Any): Array<String>
}