package org.fejoa.auth

import org.fejoa.auth.crypto.*
import org.fejoa.auth.shared.EKEConsts


class FejoaAuthProtocol(val jsonRPC: JsonRPCClient, val keyCache: BaseKeyCache) {
    fun register(algo: KDF_ALGO, url: String, user: String, password: String, iterations: Int)
            : CancelablePromise<RegisterResponseArg> {
        return keyCache.generateBaseKey(password, algo, iterations).thenSuspend { baseKey ->
            val authKey = baseKey.deriveUserKey(HASH_ALGO.SHA256)
            val response = postRPC(url, jsonRPC.call(REGISTER_METHOD, RegisterArg(user, authKey)))
            response.unsafeCast<RegisterResponseArg>()
        }
    }

    suspend fun login(url: String, user: String, password: String): CancelablePromise<Boolean> {
        val response = postRPC(url, jsonRPC.call(LOGIN_METHOD,
                EKE2LoginArg(user, EKEConsts.FEJOA_EKE2_SHA256, EKEConsts.STATE_INIT, EKEConsts.RFC5114_2048_256)))

        val initResponse = response.unsafeCast<EKE2LoginInitResponseArg>()
        val authKeyParamsRaw = initResponse.authKeyParams ?: throw Exception("Auth key parameters missing")
        val authKeyParams = parseKDFParams(authKeyParamsRaw) ?: throw Exception("Failed to parse auth key parameters")

        return keyCache.getBaseKey(password, authKeyParams.baseKeyParams).thenSuspend { baseKey ->
            val authKey = baseKey.getUserKey(parseHashAlgo(authKeyParams.algorithm), CryptoJs.fromBase64(authKeyParams.salt))
            val verifier = AuthProtocolEKE2_SHA3_256_CTR.createVerifier(CryptoJs, EKEConsts.RFC5114_2048_256,
                    CryptoJs.fromBase64(authKey.key), CryptoJs.fromBase64(initResponse.encGX),
                    CryptoJs.fromBase64(initResponse.iv))

            val encGy = verifier.getEncGy()
            val finishResponse = postRPC(url, jsonRPC.call(LOGIN_METHOD,
                    EKE2LoginArg(user, EKEConsts.FEJOA_EKE2_SHA256, EKEConsts.STATE_FINISH, EKEConsts.RFC5114_2048_256,
                            CryptoJs.toBase64(encGy.first), CryptoJs.toBase64(encGy.second),
                            CryptoJs.toBase64(verifier.getAuthToken())))).unsafeCast<EKE2LoginFinishResponseArg>()

            verifier.verify(CryptoJs.fromBase64(finishResponse.authToken))
        }
    }

    suspend fun status(url: String): StatusResponseArg {
        val response = postRPC(url, jsonRPC.call(STATUS_METHOD)).unsafeCast<StatusResponseArg>()
        return response
    }

    suspend fun logOut(url: String): String {
        val response = postRPC(url, jsonRPC.call(LOGOUT_METHOD))
                .unsafeCast<LogoutResponseArg>()
        return response.message
    }

    suspend fun getIndex(url: String): Array<IndexEntry> {
        val response = postRPC(url, jsonRPC.call(GET_INDEX_METHOD))
        val getIndexResponse = response.unsafeCast<GetIndexResponseArg>()
        return getIndexResponse.branches
    }

    suspend fun addToIndex(url: String, name: String, branch: String) {
        postRPC(url, jsonRPC.call(ADD_TO_INDEX_METHOD, AddToIndexArg(name, branch)))
    }

    suspend fun removeFromIndex(url: String, branch: String) {
        postRPC(url, jsonRPC.call(REMOVE_FROM_INDEX_METHOD, RemoveFromIndexArg(branch)))
    }

    suspend fun push(url: String, context: String, branch: String, data: ByteArray) {
        postRPC(url, jsonRPC.call(PUSH_METHOD, PushArg(context, branch, base64js.encode(data))))
    }

    suspend fun pull(url: String, context: String, branch: String): ByteArray {
        val response = postRPC(url, jsonRPC.call(PULL_METHOD, PullArg(context, branch)))
        val pullResponse = response.unsafeCast<PullResponseArg>()
        return base64js.decode(pullResponse.data)
    }
}