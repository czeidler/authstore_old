package org.fejoa.auth

import org.fejoa.auth.js.StorageArea
import org.fejoa.auth.js.await
import org.fejoa.auth.js.chrome
import kotlin.js.Json
import kotlin.js.Promise
import kotlin.js.json


interface Storage {
    fun get(key: String): Promise<Any?>
    fun store(key: String, obj: Any): Promise<Unit>
    fun delete(key: String): Promise<Unit>
}

class ChromeStorage(val storageArea: StorageArea) : Storage {
    override fun get(key: String): Promise<Any?> {
        return Promise<Json?> { resolve, _ ->
            storageArea.get(key, {items -> resolve(items) })
        }.then({result->
            if (result == null)
                return@then null
            // extract the value from the returned key value pair
            result[key]
        })
    }

    override fun store(key: String, obj: Any): Promise<Unit> {
        return Promise { resolve, reject ->
            storageArea.set(json(key to obj), {
                val lastError = chrome.runtime.lastError
                if (lastError != null)
                    reject(Exception(lastError))
                else
                    resolve(Unit)
            })}
    }

    override fun delete(key: String): Promise<Unit> {
        return Promise { resolve, reject ->
            storageArea.remove(key, {
                val lastError = chrome.runtime.lastError
                if (lastError != null)
                    reject(Exception(lastError))
                else
                    resolve(Unit)
            })}
    }
}