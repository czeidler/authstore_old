package org.fejoa.auth

import org.fejoa.auth.crypto.BaseKeyCache
import org.fejoa.auth.crypto.KDF_ALGO
import org.fejoa.auth.js.Tab
import org.fejoa.auth.js.await
import org.fejoa.auth.js.chrome
import org.fejoa.auth.js.launch
import org.w3c.dom.url.URL
import kotlin.js.json


class FejoaAuth(val jsonRPC: JsonRPCClient, val keyCache: BaseKeyCache) {
    enum class Status {
        LOGGED_OUT,
        LOGGED_IN,
        LOGGING_IN,
        REGISTERING
    }

    class StatusRefCount(val status: AuthStatus) {
        private var count = 1

        fun addRef(): Int {
            count++
            return count
        }

        fun removeRef(): Int {
            count--
            return count
        }
    }
    class AuthStatus(var url: String? = null, var status: Status = Status.LOGGED_OUT, var promise: CancelablePromise<*>? = null,
                     var user: String? = null, var statusMessage: String? = null, var errorMessage: String? = null)

    class StatusListener(val origin: String, val callback: () -> Unit)

    /**
     * origin -> StatusRefCount
     *
     * Multiple tabs can show the same origin. Only if no tabs refer to the same origin anymore, long running tasks
     * (logging in and registering) are canceled.
     */
    private val statusMap = HashMap<String, StatusRefCount>()
    val statusListeners = ArrayList<StatusListener>()

    fun getAutStatusList(): List<Pair<String, AuthStatus>> {
        return statusMap.map { it.key to it.value.status }
    }

    /**
     * Monitor open tabs and maintain the number of tabs referring to a same domain. If the ref count is 0 the
     * ongoing job, if there is one, is canceled and the status removed.
     */
    class TabMonitor(val statusMap: HashMap<String, StatusRefCount>,  val statusListeners: ArrayList<StatusListener>) {
        // tabId -> origin
        private val tabOriginMap = HashMap<Int, String>()

        /**
         * Return all tab id for a given origin
         */
        fun getTabs(origin: String): List<Int> {
            return tabOriginMap.filter { it.value == origin }.map { it.key }
        }

        init {
            chrome.tabs.query(json(), { result ->
                result.forEach { addTab(it) }
            })
            chrome.tabs.onCreated.addListener { tab ->
                addTab(tab)
            }
            chrome.tabs.onRemoved.addListener { tabId, _ ->
                val origin = tabOriginMap.remove(tabId) ?: return@addListener
                removeRef(origin)
            }
            chrome.tabs.onUpdated.addListener { tabId, _, tab ->
                val url = (tab.url ?: return@addListener).unsafeCast<String>()
                val oldOrigin = tabOriginMap[tabId]
                val newOrigin = URL(url).origin
                tabOriginMap[tabId] = newOrigin
                addRef(newOrigin)
                if (oldOrigin != null)
                    removeRef(oldOrigin)
            }
            chrome.tabs.onReplaced.addListener { addedTabId, removedTabId ->
                // the addedTabId has the new tab url/origin
                chrome.tabs.get(addedTabId, {tab ->
                    val oldOrigin = tabOriginMap.remove(removedTabId)
                    val url = tab.url ?: return@get
                    val newOrigin = URL(url).origin
                    tabOriginMap[addedTabId] = newOrigin
                    addRef(newOrigin)

                    if (oldOrigin != null)
                        removeRef(oldOrigin)
                })
            }
        }

        private fun addTab(tab: Tab) {
            val tabId = tab.id ?: return
            val url = tab.url ?: return
            val origin = URL(url).origin
            tabOriginMap[tabId] = origin
            addRef(origin)
        }

        private fun addRef(origin: String) {
            val loginStatus = statusMap[origin]
            if (loginStatus != null) {
                loginStatus.addRef()
            } else {
                statusMap[origin] = StatusRefCount(AuthStatus())
            }
        }

        private fun removeRef(origin: String) {
            val loginStatus = statusMap[origin] ?: return
            val refCount = loginStatus.removeRef()
            if (refCount == 0) {
                loginStatus.status.promise?.cancel()
                statusMap.remove(origin)

                // cleanup listeners (there shouldn't be listeners left though)
                statusListeners.removeAll { it.origin == origin }
            }
        }
    }


    // install and start the tab monitor
    val tabMonitor = TabMonitor(statusMap, statusListeners)

    fun getAuthStatus(origin: String): AuthStatus? {
        return statusMap[origin]?.status
    }

    private fun notifyStatusUpdated(origin: String, user: String?, status: Status) {
        // notify local listeners
        statusListeners.filter { it.origin == "*" || it.origin == origin }.forEach { it.callback.invoke() }

        // notify all tabs
        tabMonitor.getTabs(origin).forEach {
            chrome.tabs.sendMessage(it, json("method" to "updateLoginStatus",
                    "user" to user,
                    "authStatus" to status.name.toLowerCase()))
        }
    }

    private fun updateStatus(origin: String, status: Status = Status.LOGGED_OUT,
                             promise: CancelablePromise<*>? = null, user: String? = null,
                             statusMessage: String? = null, errorMessage: String? = null, url: String? = null) {
        val authStatus = statusMap[origin]
        if (authStatus == null) {
            // ignore; the tab is gone (don't mess up the ref count by adding a new entry now)
        } else {
            authStatus.status.status = status
            authStatus.status.url = url
            authStatus.status.promise = promise
            authStatus.status.user = user
            authStatus.status.statusMessage = statusMessage
            authStatus.status.errorMessage = errorMessage
        }
        notifyStatusUpdated(origin, user, status)
    }

    /**
     * Asynchronously start the login process
     */
    fun launchLogin(authUrl: String, user: String, password: String) {
        launch {
            val origin = URL(authUrl).origin
            val promise = FejoaAuthProtocol(jsonRPC, keyCache).login(authUrl, user, password)
            updateStatus(origin, Status.LOGGING_IN, promise, user)
            try {
                val loggedIn = promise.await()
                if (loggedIn)
                    updateStatus(origin, Status.LOGGED_IN, user = user, url = authUrl)
                else
                    updateStatus(origin, Status.LOGGED_OUT, errorMessage = "Failed to log in")
            } catch (e: dynamic) {
                val errorMessage = (e.message ?: e).unsafeCast<String>()
                updateStatus(origin, Status.LOGGED_OUT, errorMessage = errorMessage)
                throw Exception(errorMessage)
            }
        }
    }

    /**
     * Asynchronously start the register process
     */
    fun launchRegister(algo: KDF_ALGO, authUrl: String, user: String, password: String, iterations: Int) {
        launch {
            val origin = URL(authUrl).origin

            val promise = FejoaAuthProtocol(jsonRPC, keyCache).register(
                    algo, authUrl, user, password, iterations)
            updateStatus(origin, Status.REGISTERING, promise, user)
            try {
                val result = promise.await()
                if (result.user == "" || result.user != user)
                    updateStatus(origin, Status.LOGGED_OUT, errorMessage = result.message)
                else
                    updateStatus(origin, Status.LOGGED_IN, user = user, statusMessage = result.message, url = authUrl)
            } catch (e: dynamic) {
                val errorMessage = (e.message ?: e).unsafeCast<String>()
                updateStatus(origin, Status.LOGGED_OUT, errorMessage = errorMessage)
                throw Exception(errorMessage)
            }
        }
    }

    suspend fun logOut(authUrl: String): String {
        try {
            val logoutString = FejoaAuthProtocol(jsonRPC, keyCache).logOut(authUrl)
            updateStatus(authUrl)
            return logoutString
        } catch (e: dynamic) {
            val errorMessage = (e.message ?: e).unsafeCast<String>()
            throw Exception(errorMessage)
        }
    }

    /**
     * If not currently logging in or registering, query the server status
     *
     * @return the query response in case the server has been queried
     */
    suspend fun updateStatus(authUrl: String): StatusResponseArg? {
        val origin = URL(authUrl).origin
        val currentStatus = getAuthStatus(origin)
        if (currentStatus != null
                && (currentStatus.status == Status.LOGGING_IN || currentStatus.status == Status.REGISTERING)) {
            return null
        }
        try {
            val response = FejoaAuthProtocol(jsonRPC, keyCache).status(authUrl)
            if (response.currentUser != null)
                updateStatus(origin, Status.LOGGED_IN, user = response.currentUser, url = authUrl)
            else
                updateStatus(origin, Status.LOGGED_OUT)
            return response
        } catch (e: dynamic) {
            val errorMessage = (e.message ?: e).unsafeCast<String>()
            updateStatus(origin, Status.LOGGED_OUT, errorMessage = errorMessage)
            throw Exception(errorMessage)
        }
    }
}