package org.fejoa.auth.passwordmanager

import org.fejoa.auth.crypto.CryptoJs
import org.khronos.webgl.ArrayBuffer
import org.khronos.webgl.Uint8Array
import org.khronos.webgl.get


class PWGeneratorParams(val length: Int = 12, val charset: String = ALL) {
    companion object {
        val LOWER_LETTERS = "abcdefghijklmnopqrstuvwxyz"
        val CAPITAL_LETTERS = LOWER_LETTERS.toUpperCase()
        val NUMBERS = "0123456789"
        val SYMBOLS = "=+!@#$%^&*()_-|~<>,.?/:;[]{}"
        val ALL = LOWER_LETTERS + CAPITAL_LETTERS + NUMBERS + SYMBOLS
    }
}

fun generatePassword(parms: PWGeneratorParams): String {
    val charset = parms.charset
    val random = Uint8Array(CryptoJs.random(parms.length).unsafeCast<ArrayBuffer>())

    var output = ""
    (0..parms.length - 1)
            .map { i ->
                // convert the byte to a positive integer
                random[i].toInt().let { if (it < 0) it + 256 else it }
            }
            .map { it % charset.length }
            .forEach { output += charset[it] }
    return output
}