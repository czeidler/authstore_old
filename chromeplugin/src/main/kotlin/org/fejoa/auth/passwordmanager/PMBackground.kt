package org.fejoa.auth.passwordmanager

import org.fejoa.auth.js.BASIC
import org.fejoa.auth.js.chrome
import org.fejoa.auth.js.launch
import org.w3c.dom.url.URL
import kotlin.js.Json
import kotlin.js.json


class PMBackground(val accountManager: PMAccountManager) {
    private fun getCredentials(origin: String, userName: String, defaultAccountEntry: PMAccountManager.AccountEntry,
                               senderResponse: (arg: Json?) -> Unit) {
        val originCredentials = accountManager.getAccounts().filter {
            it.type == PMAccountManager.AccountEntry.Type.OPEN
        }.sortedWith(object: Comparator<PMAccountManager.AccountEntry> {
            override fun compare(a: PMAccountManager.AccountEntry, b: PMAccountManager.AccountEntry): Int {
                if (a == defaultAccountEntry)
                    return -1
                else
                    return 0
            }
        }).map { it.account }.filterNotNull().flatMap {
            it.wallets
        }.map {
            it.getPassword(origin)
        }.filterNotNull()

        val userCredentials = originCredentials.filter { it.first == userName }.firstOrNull() ?: return
        val user = userCredentials.first
        val password = userCredentials.second
        senderResponse(json("user" to user, "password" to password))
    }

    private fun storeCredentials(origin: String, message: Json, defaultAccount: PasswordManagerAccount) {
        val user = message["user"]?.unsafeCast<String>() ?: return
        val password = message["password"]?.unsafeCast<String>() ?: return
        if (user == "" || password == "")
            return

        val options = json("type" to org.fejoa.auth.js.TemplateType.BASIC,
                "title" to "Fejoa Auth Password Manager",
                "message" to "Do you want to store your password securely?",
                "iconUrl" to "images/icon.png",
                "buttons" to arrayOf(
                        json("title" to "Store password",
                                "iconUrl" to "images/ok.png"),
                        json("title" to "Ignore",
                                "iconUrl" to "images/cancel.png")
                )
        )
        val STORE_CREDENTIALS_ID = "storeCredentialsNotification"
        chrome.notifications.create(STORE_CREDENTIALS_ID, options)
        buttonClick@ chrome.notifications.onButtonClicked.addListener({ notificationId, buttonIndex ->
            if (notificationId != STORE_CREDENTIALS_ID)
                return@buttonClick
            chrome.notifications.clear(STORE_CREDENTIALS_ID)
            when (buttonIndex) {
                0 -> {
                    // TODO: use a default wallet?
                    val wallet = defaultAccount.wallets.firstOrNull() ?: return@addListener
                    wallet.putPassword(origin, user, password)
                    launch {
                        wallet.commit()
                    }
                }
            }
        })
    }

    fun start() {
        chrome.runtime.onMessage.addListener({ message, sender, senderResponse ->
            if (message == null)
                return@addListener
            if (sender.url == null)
                return@addListener
            val origin = URL(sender.url).origin
            val defaultAccountEntry = accountManager.getDefaultAccount() ?: return@addListener
            val defaultAccount = defaultAccountEntry.account?: return@addListener

            val requestName = message["method"] ?: return@addListener
            if (requestName == "getCredentials") {
                val userName = message["user"].unsafeCast<String>()
                getCredentials(origin, userName, defaultAccountEntry, senderResponse)
            } else if (requestName == "storeCredentials") {
                storeCredentials(origin, message, defaultAccount)
            }
        })
    }

    fun fillRegisterPassword(password: String) {
        chrome.tabs.getSelected(null, { tab ->
            if (tab.id == null)
                return@getSelected
            chrome.tabs.sendMessage(tab.id, json("method" to "fillRegisterPassword",
                    "password" to password))
        })
    }
}