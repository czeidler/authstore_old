package org.fejoa.auth.passwordmanager

import org.fejoa.auth.*
import org.fejoa.auth.crypto.*
import org.fejoa.auth.database.StorageManager
import org.fejoa.auth.js.Object
import org.fejoa.auth.js.await
import org.fejoa.auth.js.launch
import kotlin.js.json


class PMAccountManager(val storageManager: StorageManager) {
    private val INDEX_NAME = "account.index"
    private var index = Index()
    val accountListeners = ArrayList<() -> Unit>()
    private val accounts = ArrayList<AccountEntry>()

    class AccountEntry(val indexEntry: Index.Entry, var type: Type, var account: PasswordManagerAccount? = null,
                       var promise: CancelablePromise<*>? = null) {
        enum class Type {
            CLOSED,
            OPEN,
            OPENING, // in the process of being opened
            CREATING // in the process of being created
        }

        fun getAccountName(): String {
            return indexEntry.name
        }
    }

    class Index(var default: String = "", val accounts: ArrayList<Entry> = ArrayList()) {
        class Entry(val name: String, val branch: String)
        companion object {
            fun fromJson(json: dynamic): Index {
                val accounts = ArrayList<Entry>()
                for (i in 0..json.accounts.length - 1) {
                    val accountEntry = json.accounts[i].unsafeCast<Entry>()
                    accounts.add(Entry(accountEntry.name, accountEntry.branch))
                }
                return Index(json.default, accounts)
            }
        }


        fun toJson(): Any {
            // complicated but more reliable way to convert Kotlin object to js
            return JSON.parse(JSON.stringify(this))
        }

        fun removeAccount(accountName: String) {
            val index = accounts.indexOfFirst { it.name == accountName }
            if (index >= 0)
                accounts.removeAt(index)
        }

        fun getAccountEntry(accountName: String): Entry? {
            return accounts.firstOrNull { it.name == accountName }
        }
    }

    fun getAccount(name: String): AccountEntry? {
        return accounts.firstOrNull { it.indexEntry.name == name}
    }

    fun getAccounts(): List<AccountEntry> {
        return accounts
    }

    suspend fun readExistingAccounts() {
        if (index.accounts.size != 0)
            return
        val indexRaw = storageManager.readPlain(INDEX_NAME).await() ?: return
        index = Index.fromJson(indexRaw.unsafeCast<String>())
        // fill index entries into the account entry list
        for (accountEntry in index.accounts) {
            val existingAccount = getAccount(accountEntry.name)
            if (existingAccount != null)
                continue
            accounts.add(AccountEntry(accountEntry, AccountEntry.Type.CLOSED))
        }
        if (index.accounts.size > 0)
            notifyAccountsUpdated()
    }

    fun notifyAccountsUpdated() {
        accountListeners.forEach { it.invoke() }
    }

    suspend private fun storeIndex() {
        storageManager.writePlain(INDEX_NAME, index.toJson()).await()
    }

    private fun updateAccountEntry(indexEntry: Index.Entry, type: AccountEntry.Type,
                                   account: PasswordManagerAccount? = null,
                                   promise: CancelablePromise<*>? = null): AccountEntry {
        var accountEntry = getAccount(indexEntry.name)
        if (accountEntry == null) {
            accountEntry = AccountEntry(indexEntry, type, account, promise)
            accounts.add(accountEntry)
        } else {
            accountEntry.type = type
            accountEntry.account = account
            accountEntry.promise = promise
        }
        notifyAccountsUpdated()
        return accountEntry
    }

    private fun removeAccountEntry(accountName: String) {
        val accountEntry = getAccount(accountName) ?: return
        accounts.remove(accountEntry)
        notifyAccountsUpdated()
    }

    suspend fun createAccount(accountName: String, password: String, algo: KDF_ALGO, nIterations: Int)
            : PasswordManagerAccount {
        val salt = base64js.encode(CryptoHelper(CryptoJs).generateSalt16())
        val params = PDKDF2Params(algo.name, nIterations, salt)
        return createAccount(accountName, password, params)
    }

    suspend fun createAccount(accountName: String, password: String, kdfParams: KDFParams): PasswordManagerAccount {
        val name = accountName.trim()
        if (name == "")
            throw Exception("Invalid account name")
        if (getAccount(accountName) != null)
            throw Exception("Account already exists")

        val id = CryptoHelper(CryptoJs).generateId()
        val promise = storageManager.create(id, name, password, kdfParams)

        val accountEntry = updateAccountEntry(Index.Entry(name, id), AccountEntry.Type.CREATING, null, promise)

        try {
            val storageDir = StorageDir(promise.await())
            val account = PasswordManagerAccount.create(storageManager, storageDir, name)
            account.commit()
            index.accounts.add(accountEntry.indexEntry)
            if (index.default == "")
                index.default = name
            storeIndex()

            updateAccountEntry(accountEntry.indexEntry, AccountEntry.Type.OPEN, account)
            return account
        } catch (e: Exception) {
            removeAccountEntry(accountName)
            throw e
        }
    }

    fun getDefaultAccount(): AccountEntry? {
        return getAccount(index.default)
    }

    suspend fun openAccount(accountName: String, password: String): PasswordManagerAccount? {
        val name = accountName.trim()
        val branch = index.getAccountEntry(name)?.branch ?: return null

        val promise = storageManager.open(branch, name, password) ?: return null
        val indexEntry = Index.Entry(accountName, branch)
        updateAccountEntry(indexEntry, AccountEntry.Type.OPENING, null, promise)

        return try {
            val storageDir = StorageDir(promise.await())
            val account = PasswordManagerAccount(storageManager, storageDir, name)
            updateAccountEntry(indexEntry, AccountEntry.Type.OPEN, account)
            account
        }  catch (e: dynamic) {
            // failed to decrypt or canceled
            updateAccountEntry(indexEntry, AccountEntry.Type.CLOSED)
            null
        }
    }

    fun closeAccount(accountName: String) {
        val accountEntry = getAccount(accountName) ?: return
        updateAccountEntry(accountEntry.indexEntry, AccountEntry.Type.CLOSED)
    }

    fun closeAccount(account: PasswordManagerAccount) {
        closeAccount(account.name)
    }

    suspend fun deleteAccount(accountName: String) {
        val accountEntry = getAccount(accountName) ?: return
        val account = accountEntry.account
        accounts.remove(accountEntry)
        index.removeAccount(accountName)
        storeIndex()
        storageManager.delete(accountEntry.indexEntry.branch, accountName).await()
        notifyAccountsUpdated()
    }

    suspend fun import(protocol: FejoaAuthProtocol, remote: Remote, indexEntry: IndexEntry) {
        val mainBranch = protocol.pull(remote.url, indexEntry.name, indexEntry.branch)
        val json = JSON.parse<dynamic>(TextDecoder().decode(mainBranch))
        storageManager.writeRaw(indexEntry.branch, indexEntry.name, json).await()

        val accountEntry = updateAccountEntry(Index.Entry(indexEntry.name, indexEntry.branch), AccountEntry.Type.CLOSED)

        index.accounts.add(accountEntry.indexEntry)
        if (index.default == "")
            index.default = indexEntry.name
        storeIndex()
    }
}

class PasswordManagerAccount(val storageManager: StorageManager, storageDir: StorageDir, val name: String)
    : UserData(storageDir, storageDir.getBranch()) {
    val walletUpdatedListeners = ArrayList<() -> Unit>()

    init {
        launch {
            readWallets()
        }
    }

    private fun notifyWalletUpdated() {
        walletUpdatedListeners.forEach { it.invoke() }
    }

    companion object {
        suspend fun create(storageManager: StorageManager, storage: StorageDir, name: String): PasswordManagerAccount {
            val account = PasswordManagerAccount(storageManager, storage, name)
            account.createWallet()
            return account
        }
    }

    class Wallet(val storageDir: StorageDir) {
        fun getId(): String {
            return storageDir.getBranch()
        }

        fun putPassword(origin: String, username: String, password: String) {
            storageDir.put("passwords/" + origin, json("username" to username, "password" to password))
        }

        fun getPassword(origin: String): Pair<String, String>? {
            val entry = (storageDir.get("passwords/" + origin) ?: return null).asDynamic()
            return entry.username.unsafeCast<String>() to entry.password.unsafeCast<String>()
        }

        fun getPasswordOrigins(): Array<String> {
            return storageDir.list("passwords")
        }

        suspend fun commit() {
            storageDir.commit()
        }
    }

    suspend fun createWallet(): Wallet {
        val id = CryptoHelper(CryptoJs).generateId()
        val key = CryptoHelper(CryptoJs).generateKey(32, CryptoSettings.Symmetric())
        val storageDir = storageManager.createOrOpen(id, name, key)
        val wallet = Wallet(storageDir)
        wallet.commit()
        val branchInfo = BranchInfo(id, "Wallet", "pm/wallets", key)
        addBranch(branchInfo)

        addWallet(wallet)
        return wallet
    }

    val wallets = ArrayList<Wallet>()

    fun getWallet(id: String): Wallet? {
        return wallets.firstOrNull { it.getId() == id }
    }

    private fun addWallet(wallet: Wallet) {
        wallets.add(wallet)
    }

    suspend fun readWallets() {
        wallets.clear()
        val entries = listBranches("pm/wallets")
        entries.forEach {
            val wallet = readWallet(it.branch, it.key)
            if (wallet != null && getWallet(wallet.getId()) == null)
                addWallet(wallet)
        }
        notifyWalletUpdated()
    }

    suspend fun readWallet(branch: String, key: CryptoKey): Wallet? {
        val storageDir = storageManager.createOrOpen(branch, name, key)
        return Wallet(storageDir)
    }
}
