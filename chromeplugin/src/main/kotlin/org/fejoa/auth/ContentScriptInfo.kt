package org.fejoa.auth

import org.fejoa.auth.js.chrome
import org.w3c.dom.url.URL
import kotlin.js.Json
import kotlin.js.Promise
import kotlin.js.json


class FejoaAuthPageInfo(val url: URL, val authPath: String, val passwordFormType: String) {
    fun getFejoaAuthPath(): String {
        return "${url.protocol}//${url.host}/$authPath"
    }

    fun origin(): String {
        return url.origin
    }

    companion object {
        fun getCurrentPage(): Promise<Pair<FejoaAuthPageInfo?, String?>> {
            return Promise({ resolve, _ ->
                chrome.tabs.getSelected(null, { tab ->
                    val tabId = tab.id ?: return@getSelected resolve(null to "No tab id")
                    if (tabId < 0)
                        return@getSelected resolve(null to "No to selected")

                    chrome.tabs.sendMessage(tabId, json("method" to "getPageInfo"), null, { response ->
                        try {
                            if (response == null)
                                return@sendMessage resolve(null to "Failed to query content page")
                            // portal info
                            val portalInfo = response["portalInfo"]?.unsafeCast<Json>() ?: return@sendMessage resolve(null to "No portal info")
                            val status = portalInfo["status"] ?:
                                    return@sendMessage resolve(null to "Failed to query content page")
                            if (status != 0) {
                                val message = portalInfo["message"]?.unsafeCast<String>() ?: "Unknown error"
                                return@sendMessage resolve(null to message)
                            }
                            val path = portalInfo["path"]?.unsafeCast<String>() ?:
                                    return@sendMessage resolve(null to "Fejoa auth portal path missing")

                            // tab url
                            val url = tab.url ?: return@sendMessage resolve(null to "Tab has no url")

                            // password form info
                            val passWordFormType = response["passwordFormType"]?.unsafeCast<String>() ?: return@sendMessage resolve(null to "No passwordFormType info")

                            // send result
                            resolve(FejoaAuthPageInfo(URL(url), path, passWordFormType) to null)
                        } catch (e: dynamic) {
                            resolve(null to (e.message ?: e))
                        }
                    })
                })
            })
        }
    }
}