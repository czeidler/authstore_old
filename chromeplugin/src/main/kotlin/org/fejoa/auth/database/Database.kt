package org.fejoa.auth.database

import org.fejoa.auth.CancelablePromise
import org.fejoa.auth.KDFParams
import org.fejoa.auth.Storage
import org.fejoa.auth.UserKey
import org.fejoa.auth.crypto.*
import org.fejoa.auth.js.Object
import org.fejoa.auth.js.await
import kotlin.js.json


interface Database {
    fun getBranch(): String
    fun put(path: String, data: Any)
    fun get(path: String): Any?
    fun getString(path: String): String?
    fun putByteArray(path: String, data: ByteArray)
    fun getByteArray(path: String): ByteArray?
    fun list(path: String = ""): Array<String>
    suspend fun commit()
}

abstract class JsonBackedDatabase(protected val storagePair: Pair<Storage, String>, val branch: String)
    : Database {
    protected var jsonStorage: dynamic = json()

    override fun getBranch(): String {
        return branch
    }

    override fun put(path: String, data: Any) {
        val parts = path.split("/")
        var current = jsonStorage
        for (i in 0..parts.size - 2) {
            val part = parts[i]
            var child = current[part]
            if (child == undefined) {
                child = json()
                current[part] = child
            }
            current = child
        }
        current[parts.last()] = data
    }

    private fun getDir(parts: List<String>, depth: Int): dynamic {
        var current = jsonStorage
        for (i in 0..depth - 1) {
            current = current[parts[i]]
            if (current == undefined)
                return null
        }
        return current
    }

    override fun get(path: String): Any? {
        val parts = path.split("/")
        var dir = getDir(parts, parts.size - 1) ?: return null
        val result = dir[parts.last()]
        if (result == undefined)
            return null
        return result.unsafeCast<Any?>()
    }

    override fun getString(path: String): String? {
        return get(path)?.unsafeCast<String>()
    }

    override fun putByteArray(path: String, data: ByteArray) {
        put(path, base64js.encode(data))
    }

    override fun getByteArray(path: String): ByteArray? {
        val data = getString(path) ?: return null
        return base64js.decode(data)
    }

    override fun list(path: String): Array<String> {
        if (path == "")
            return Object.keys(jsonStorage)
        val parts = path.split("/")
        var entry: dynamic = getDir(parts, parts.size) ?: return Array(0, {""})
        val keys = Object.keys(entry)
        return keys
    }
}


class EncryptedDatabase(storagePair: Pair<Storage, String>, branch: String, private val cryptoKey: CryptoKey): JsonBackedDatabase(storagePair, branch) {
    var cryptoSettings = CryptoSettings.Symmetric()

    companion object {
        suspend fun open(storagePair: Pair<Storage, String>, branch: String, cryptoKey: CryptoKey): EncryptedDatabase? {
            val (storage, key) = storagePair
            val envelope = storage.get(key).await()?.
                    unsafeCast<EncryptedEnvelope.Envelope>() ?: return null
            val plain = EncryptedEnvelope().unpack(cryptoKey, envelope)
            val data = JSON.parse<dynamic>(TextDecoder().decode(plain))

            val database = EncryptedDatabase(storagePair, branch, cryptoKey)
            database.cryptoSettings = envelope.encParams.toSymmetric()
            database.jsonStorage = data
            return database
        }
    }

    suspend fun toEnvelope(): EncryptedEnvelope.Envelope {
        return EncryptedEnvelope().pack(cryptoKey, cryptoSettings, TextEncoder().encode(JSON.stringify(jsonStorage)))
    }

    override suspend fun commit() {
        val (storage, key) = storagePair
        storage.store(branch, toEnvelope()).await()
    }
}

class PasswordProtectedDatabase private constructor(storagePair: Pair<Storage, String>, branch: String, val userKey: UserKey)
    : JsonBackedDatabase(storagePair, branch) {
    companion object {
        suspend fun open(storagePair: Pair<Storage, String>, branch: String, password: String, keyCache: BaseKeyCache)
                : CancelablePromise<PasswordProtectedDatabase>? {
            val (storage, key) = storagePair
            val envelope = storage.get(key).await()?.
                    unsafeCast<PasswordProtectedEnvelope.Envelope>() ?: return null

            val passwordEnvelope = PasswordProtectedEnvelope()
            val unpackPromise = passwordEnvelope.unpack(password, keyCache, envelope)

            return unpackPromise.then {
                val database = PasswordProtectedDatabase(storagePair, branch, it.second)
                database.jsonStorage = JSON.parse(TextDecoder().decode(it.first))
                database
            }
        }

        suspend fun create(storagePair: Pair<Storage, String>, branch: String, password: String, kdfParams: KDFParams,
                                  keyCache: BaseKeyCache) : CancelablePromise<PasswordProtectedDatabase> {
            val baseKeyPromise = keyCache.getBaseKey(password, kdfParams)

            return baseKeyPromise.thenSuspend {
                val userKey = it.deriveUserKey(HASH_ALGO.SHA256)
                val database = PasswordProtectedDatabase(storagePair, branch, userKey)
                database
            }
        }
    }

    suspend fun toEnvelope(): PasswordProtectedEnvelope.Envelope {
        return PasswordProtectedEnvelope().pack(userKey, TextEncoder().encode(JSON.stringify(jsonStorage)))
    }

    suspend override fun commit() {
        val (storage, key) = storagePair
        storage.store(key, toEnvelope()).await()
    }
}