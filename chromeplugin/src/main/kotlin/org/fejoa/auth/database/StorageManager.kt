package org.fejoa.auth.database

import org.fejoa.auth.*
import org.fejoa.auth.crypto.*
import org.fejoa.auth.js.chrome
import kotlin.js.Promise


class StorageManager(val keyCache: BaseKeyCache) {
    val storage: Storage = ChromeStorage(chrome.storage.local)

    fun readPlain(key: String): Promise<Any?> {
        return storage.get(key)
    }

    fun writePlain(key: String, data: Any): Promise<Unit> {
        return storage.store(key, data)
    }

    fun delete(branch: String, context: String): Promise<Unit> {
        return storage.delete(toKey(branch, context))
    }

    fun writeRaw(branch: String, context: String, data: Any): Promise<Unit> {
        return storage.store(toKey(branch, context), data)
    }

    private fun toKey(branch: String, context: String): String {
        return branch
    }

    suspend fun create(branch: String, context: String, password: String, algo: KDF_ALGO, nIterations: Int)
            : CancelablePromise<PasswordProtectedDatabase> {
        val salt = base64js.encode(CryptoHelper(CryptoJs).generateSalt16())
        val params = PDKDF2Params(algo.name, nIterations, salt)
        return create(branch, context, password, params)
    }

    suspend fun create(branch: String, context: String, password: String, kdfParams: KDFParams)
            : CancelablePromise<PasswordProtectedDatabase> {
        return PasswordProtectedDatabase.create(storage to toKey(branch, context), branch, password, kdfParams, keyCache)
    }

    suspend fun open(branch: String, context: String, password: String)
            : CancelablePromise<PasswordProtectedDatabase>? {
        return PasswordProtectedDatabase.open(storage to toKey(branch, context), branch, password, keyCache)
    }

    suspend fun createOrOpen(branch: String, context: String, cryptoKey: CryptoKey): StorageDir {
        val opened = EncryptedDatabase.open(storage to toKey(branch, context), branch, cryptoKey)
        if (opened != null)
            return StorageDir(opened)
        return StorageDir(EncryptedDatabase(storage to toKey(branch, context), branch, cryptoKey))
    }
}