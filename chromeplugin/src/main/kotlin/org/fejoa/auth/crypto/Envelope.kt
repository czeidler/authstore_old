package org.fejoa.auth.crypto

import org.fejoa.auth.CancelablePromise
import org.fejoa.auth.UserKey
import org.fejoa.auth.UserKeyParams


/**
 * Uses a password to encrypt the containing data
 */
class PasswordProtectedEnvelope {
    val crypto = CryptoJs

    // To be stored as a json string
    class Envelope(val keyParams: UserKeyParams, val encParams: SymmetricJson, val encIV: String,
                   val cipher: String)

    suspend fun pack(userKey: UserKey, data: ByteArray): Envelope {
        // TODO: use a master key and encrypt it with the user key
        val container = Container.create(crypto.secretKey(base64js.decode(userKey.key),
                CryptoSettings.Symmetric()))
        val cipher = container.encrypt(data)
        return Envelope(userKey.params, container.settings.toJsonReady(), base64js.encode(container.iv),
                base64js.encode(cipher))
    }

    /**
     * Returns the unpacked data and the extracted user key
     */
    suspend fun unpack(password: String, baseKeyCache: BaseKeyCache, pack: Envelope): CancelablePromise<Pair<ByteArray, UserKey>> {
        val userKeyParams = parseKDFParams(pack.keyParams) ?: throw Exception("Invalid user key parameters")
        return baseKeyCache.getBaseKey(password, userKeyParams.baseKeyParams).thenSuspend { baseKey ->
            val userKey = baseKey.getUserKey(parseHashAlgo(pack.keyParams.algorithm),
                    base64js.decode(pack.keyParams.salt))
            val encParams = pack.encParams.toSymmetric()
            val key = crypto.secretKey(base64js.decode(userKey.key), encParams)
            val container = Container(key, base64js.decode(pack.encIV), encParams)
            container.decrypt(base64js.decode(pack.cipher)) to userKey
        }
    }
}

class EncryptedEnvelope {
    val crypto = CryptoJs

    // To be stored as a json string
    class Envelope(val encParams: SymmetricJson, val encIV: String, val cipher: String)

    suspend fun pack(key: CryptoKey, settings: CryptoSettings.Symmetric, data: ByteArray): Envelope {
        val container = Container.create(key, settings)
        val cipher = container.encrypt(data)
        return Envelope(container.settings.toJsonReady(), base64js.encode(container.iv), base64js.encode(cipher))
    }

    /**
     * Returns the unpacked data and the extracted user key
     */
    suspend fun unpack(key: CryptoKey, pack: Envelope): ByteArray {
        val encParams = pack.encParams.toSymmetric()
        val container = Container(key, base64js.decode(pack.encIV), encParams)
        return container.decrypt(base64js.decode(pack.cipher))
    }
}