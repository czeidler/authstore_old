/*
 * Copyright 2017.
 * Distributed under the terms of the GPLv3 License.
 *
 * Authors:
 *      Clemens Zeidler <czei002@aucklanduni.ac.nz>
 */
package org.fejoa.auth.crypto

import org.khronos.webgl.ArrayBuffer
import org.khronos.webgl.Uint8Array


/**
 * Zero knowledge proof that the prover knows a secret. The prover does not reveal the secret if the verifier does not
 * know the secret. Moreover, the verifier can't brute force the prover's secret from the exchanged data.

 * This is based on EKE2 (Mihir Bellare, David Pointchevaly and Phillip Rogawayz. Authenticated key exchange secure
 * against dictionary attacks)

 * Outline of the protocol:
 * - Enc(), Dec() are encryption/decryption methods using the secret as key
 * - H is a hash function
 * 1) ProverState0 generates random value x and calculates:
 * gx = g.modPow(x, p)
 * encGX = Enc(g.modPow(x, p))
 * The prover sends encGX to the Verifier:
 * ProverState0 -> encGX -> Verifier
 * 2) The verifier generates random value y and calculates:
 * gx = Dec(encGX)
 * gy = g.modPow(y, p)
 * gxy = gx.modPow(Y, p)
 * sk' = gx || gy || gxy
 * authVerifier = H(sk' || 1) (used to by the prover to verify that the verifier knows the secret)
 * The verifier sends Enc(gy) and authVerifier to the prover:
 * Verifier -> Enc(gy),authVerifier -> ProverState1
 * 3) ProverState1 calculates:
 * gy = Dec(encGY)
 * gxy = gy.modPow(x, p)
 * sk' = gx || gy || gxy
 * authVerifier = H(sk' || 1)
 * authProver = H(sk' || 2)
 * The prover checks that authVerifier matches with the received value and sends authProver to the verifier:
 * ProverState1 -> authProver -> Verifier
 * 4) The verifier calculates:
 * authProver = H(sk' || 2)
 * If authProver matches the received value the authentication succeeded.
 */
class AuthProtocolEKE2_SHA3_256_CTR<SymKey> private constructor(val crypto: CryptoInterface<SymKey>, encGroup: String, val secret: ByteArray) {
    private val g: BigInteger
    private val p: BigInteger
    private var secretKey: SymKey? = null

    // the ids are not configurable at the moment
    internal val proverId = "prover"
    internal val verifierId = "verifier"

    val symmetric = CryptoSettings.Symmetric(
            algorithm = SYM_ALGO.AES_CTR,
            keyType = "AES",
            keySize = 256,
            ivSize = 16 * 8
    )

    init {
        val parameters: DHParameters
        when (encGroup) {
            DH_GROUP.RFC5114_2048_256.name -> {
                parameters = DHStandardGroups.rfc5114_2048_256
            }

            else -> throw Exception("Unsupported group: " + encGroup)
        }
        g = parameters.g
        p = parameters.p
    }

    private suspend fun getSecretKey(): SymKey {
        secretKey.let { if (it != null) return it}
        val key = crypto.secretKey(secret, symmetric)
        secretKey = key
        return key
    }

    /**
     * @return the encrypted data and the iv used for encryption
     */
    internal suspend fun encrypt(data: BigInteger, secret: SymKey): Pair<ByteArray, ByteArray> {
        val iv = CryptoHelper(crypto).generateSalt16()
        val byteArray = Uint8Array(hexToBytes(data.toString(16))).unsafeCast<ByteArray>()
        return crypto.encryptSymmetric(byteArray, secret, iv, symmetric) to iv
    }

    internal suspend fun decrypt(data: ByteArray, secret: SymKey, iv: ByteArray): BigInteger {
        val plain = crypto.decryptSymmetric(data, secret, iv, symmetric)
        val hex = bytesToHex(Uint8Array(plain.unsafeCast<ArrayBuffer>()).unsafeCast<Array<Byte>>())
        return bigInt(hex, 16)
    }

    private suspend fun getSessionKeyPrime(gx: BigInteger, gy: BigInteger, gxy: BigInteger): ByteArray {
        return hash(proverId + verifierId + gx.toString(16) + gy.toString(16) + gxy.toString(16))
    }

    private suspend fun getVerifierAuthToken(sessionKeyPrime: ByteArray): ByteArray {
        return hash(crypto.hex(sessionKeyPrime) + "1")
    }

    private suspend fun getProverAuthToken(sessionKeyPrime: ByteArray): ByteArray {
        return hash(crypto.hex(sessionKeyPrime) + "2")
    }

    inner class ProverState0 {
        private val x: BigInteger = bigInt(crypto.hex(crypto.random(16)), 16)
        private val gx: BigInteger

        init {
            gx = g.modPow(x, p)
        }

        /**
         * @return the encrypted gx and the iv used for encryption
         */
        suspend fun getEncGX(): Pair<ByteArray, ByteArray> {
            return encrypt(gx, getSecretKey())
        }

        suspend fun setVerifierResponse(encGY: ByteArray, iv: ByteArray, authToken: ByteArray): ProverState1? {
            val gy = decrypt(encGY, getSecretKey(), iv)
            val gxy = gy.modPow(x, p)
            val sessionKeyPrime = getSessionKeyPrime(gx, gy, gxy)
            val expectedVerifierToken = getVerifierAuthToken(sessionKeyPrime)
            if (!crypto.isEqual(expectedVerifierToken, authToken))
                return null
            return ProverState1(sessionKeyPrime)
        }
    }

    inner class ProverState1 constructor(internal val sessionKeyPrime: ByteArray) {
        suspend fun getAuthToken(): ByteArray {
            return getProverAuthToken(sessionKeyPrime)
        }
    }

    inner class Verifier
    constructor(val encGX: ByteArray, val iv: ByteArray) {
        private val y = bigInt(crypto.hex(crypto.random(16)), 16)
        private val gy: BigInteger = g.modPow(y, p)
        // sessionKey'
        private var sessionKeyPrime: ByteArray? = null

        suspend private fun calculateSessionKeyPrime(): ByteArray {
            sessionKeyPrime.let { if (it != null) return it }
            val gx = decrypt(encGX, getSecretKey(), iv)
            val gxy = gx.modPow(y, p)
            val keyPrime = getSessionKeyPrime(gx, gy, gxy)
            sessionKeyPrime = keyPrime
            return keyPrime
        }


        suspend fun getEncGy(): Pair<ByteArray, ByteArray> {
            return encrypt(gy, getSecretKey())
        }

        suspend fun getAuthToken(): ByteArray {
            return getVerifierAuthToken(calculateSessionKeyPrime())
        }

        suspend fun verify(proverToken: ByteArray): Boolean {
            val expectedToken = getProverAuthToken(calculateSessionKeyPrime())
            return crypto.isEqual(expectedToken, proverToken)
        }
    }

    private fun createProver(): ProverState0 {
        return ProverState0()
    }

    private fun createVerifier(encGX: ByteArray, iv: ByteArray): Verifier {
        return Verifier(encGX, iv)
    }

    private suspend fun hash(data: String): ByteArray {
        return crypto.hash(TextEncoder().encode(data), HASH_ALGO.SHA256)
    }

    companion object {
        fun <SymKey>createProver(crypto: CryptoInterface<SymKey>, encGroup: String, secret: ByteArray)
                : AuthProtocolEKE2_SHA3_256_CTR<SymKey>.ProverState0 {
            return AuthProtocolEKE2_SHA3_256_CTR(crypto, encGroup, secret).createProver()
        }

        fun <SymKey>createVerifier(crypto: CryptoInterface<SymKey>, encGroup: String, secret: ByteArray,
                                   encGX: ByteArray, iv: ByteArray): AuthProtocolEKE2_SHA3_256_CTR<SymKey>.Verifier {
            return AuthProtocolEKE2_SHA3_256_CTR(crypto, encGroup, secret).createVerifier(encGX, iv)
        }
    }
}
