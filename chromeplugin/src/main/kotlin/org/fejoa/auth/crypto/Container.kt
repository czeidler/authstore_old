package org.fejoa.auth.crypto


class Container(val key: CryptoKey, val iv: ByteArray, val settings: CryptoSettings.Symmetric) {
    companion object {
        suspend fun create(key: CryptoKey? = null, settings: CryptoSettings.Symmetric? = null): Container {
            val set = settings ?: CryptoSettings.Symmetric()
            val iv = CryptoJs.random(set.ivSize / 8)
            return Container(key ?: CryptoJs.secretKey(CryptoJs.random(set.keySize / 8), set), iv, set)
        }
    }

    suspend fun encrypt(data: ByteArray): ByteArray {
        return CryptoJs.encryptSymmetric(data, key, iv, settings)
    }

    suspend fun decrypt(cipher: ByteArray): ByteArray {
        return CryptoJs.decryptSymmetric(cipher, key, iv, settings)
    }
}