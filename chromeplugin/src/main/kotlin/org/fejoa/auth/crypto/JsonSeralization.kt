package org.fejoa.auth.crypto

import org.fejoa.auth.PDKDF2Params
import org.fejoa.auth.UserKeyParams


class SymmetricJson(val algorithm: String, val keyType: String, val keySize: Int, val ivSize: Int)

fun CryptoSettings.Symmetric.toJsonReady(): SymmetricJson {
    return SymmetricJson(algorithm.javaName, keyType, keySize, ivSize)
}

fun SymmetricJson.toSymmetric(): CryptoSettings.Symmetric {
    return CryptoSettings.Symmetric(parseSymAlgo(algorithm), keyType, keySize, ivSize)
}

fun parseKDFParams(authKeyParams: UserKeyParams): UserKeyParams?  {
    val baseKeyParamsRaw: dynamic = authKeyParams.baseKeyParams
    when (baseKeyParamsRaw.algorithm) {
        KDF_ALGO.PBKDF2WithSHA256.name -> {
            val params = baseKeyParamsRaw.unsafeCast<PDKDF2Params>()
            // create a new params object with the correct type information (a cast is not enough)
            authKeyParams.baseKeyParams = PDKDF2Params(params.algorithm, params.iterations, params.salt)
        }
        else -> return null
    }
    return authKeyParams
}