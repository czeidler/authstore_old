package org.fejoa.auth.crypto

import org.fejoa.auth.js.await
import org.khronos.webgl.ArrayBuffer
import org.khronos.webgl.Uint8Array
import org.w3c.dom.Window
import org.w3c.dom.get
import kotlin.browser.window
import kotlin.js.Json
import kotlin.js.Promise
import kotlin.js.json


external open class BigInteger {
    constructor(argument: Any, base: Int = definedExternally)
    fun modPow(exp: Any, mod: Any): BigInteger
    fun toString(radix: Int): String
}
external fun bigInt(argument: Any, radix: Int): BigInteger


external open class TextEncoder {
    constructor()

    fun encode(text: String, option: Json = definedExternally): ByteArray
}

external open class TextDecoder {
    constructor()

    fun decode(buffer: ByteArray, option: Json = definedExternally): String
}

external interface CryptoKey

external interface SubtleCrypto {
    fun digest(algo: String, buffer: ByteArray): Promise<ByteArray>
    fun generateKey(algo: Json, extractable: Boolean, keyUsages: Array<String>): Promise<CryptoKey>

    fun deriveKey(algo: Json, masterKey: CryptoKey, derivedKeyAlgo: Json, extractable: Boolean,
                  keyUsages: Array<String>): Promise<CryptoKey>

    fun encrypt(algorithm: Json, key: CryptoKey, data: ByteArray): Promise<ByteArray>
    fun decrypt(algorithm: Json, key: CryptoKey, data: ByteArray): Promise<ByteArray>

    fun importKey(format: String, keyData: ByteArray, algo: Json, extractable: Boolean,
                  usages: Array<String>): Promise<CryptoKey>
    fun exportKey(format: String, key: CryptoKey): Promise<Any>
}

external interface Crypto {
    val subtle: SubtleCrypto
    fun getRandomValues(typedArray: Any)
}

fun Window.crypto(): Crypto {
    return window["crypto"].unsafeCast<Crypto>()
}

@JsName("hex")
external fun hexJs(buffer: ByteArray): String

external fun hexToBytes(hex: String): Array<Byte>
external fun bytesToHex(bytes: Array<Byte>): String


external object base64js {
    fun encode(buffer: ByteArray): String
    fun decode(str: String): ByteArray
}

object CryptoJs : CryptoInterface<CryptoKey> {
    override fun hex(data: ByteArray): String {
        return hexJs(data)
    }

    override suspend fun secretKey(secret: ByteArray, symSettings: CryptoSettings.Symmetric): CryptoKey {
        return window.crypto().subtle.importKey("raw",
                secret,
                json("name" to symSettings.algorithm.jsName),
                true,
                arrayOf("encrypt", "decrypt")).await()
    }

    override suspend fun encryptSymmetric(plain: ByteArray, key: CryptoKey, iv: ByteArray, settings: CryptoSettings.Symmetric): ByteArray {
        val algorithm = json("name" to settings.algorithm.jsName,
                "counter" to iv,
                "length" to 128)
        return window.crypto().subtle.encrypt(algorithm, key, plain).await()
    }

    /**
     * Workaround for js <-> kotlin array mixups...
     */
    override fun isEqual(array1: ByteArray, array2: ByteArray): Boolean {
        return hex(array1) == hex(array2)
    }

    override suspend fun decryptSymmetric(cipher: ByteArray, key: CryptoKey, iv: ByteArray, settings: CryptoSettings.Symmetric): ByteArray {
        val algorithm = json("name" to settings.algorithm.jsName, "counter" to iv, "length" to 128)
        return window.crypto().subtle.decrypt(algorithm, key, cipher).await()
    }

    override fun toBase64(data: ByteArray): String {
        return base64js.encode(data)
    }

    override fun fromBase64(base64: String): ByteArray {
        return base64js.decode(base64)
    }

    override suspend fun hash(data: ByteArray, algorithm: HASH_ALGO): ByteArray {
        return window.crypto().subtle.digest(algorithm.nameJs, data).await()
    }

    override fun random(size: Int): ByteArray {
        var array = Uint8Array(ArrayBuffer(size))
        window.crypto().getRandomValues(array)
        return array.buffer.unsafeCast<ByteArray>()
    }

    override suspend fun deriveKeyPBKDF2WithSHA256(password: String, salt: ByteArray, iterations: Int, keyTyp: SYM_ALGO): CryptoKey {
        val passwordKey = window.crypto().subtle.importKey(
                "raw",
                TextEncoder().encode(password),
                json("name" to "PBKDF2"),
                false,
                arrayOf("deriveBits", "deriveKey")
        ).await()
        return window.crypto().subtle.deriveKey(
                json("name" to "PBKDF2",
                    "salt" to salt,
                    "iterations" to iterations,
                    "hash" to "SHA-256"
                ),
                passwordKey,
                json("name" to keyTyp.jsName, "length" to 256),
                true,
                arrayOf("encrypt", "decrypt")
        ).await()
    }

    override suspend fun keyBytes(key: CryptoKey): ByteArray {
        return window.crypto().subtle.exportKey("raw", key).await().unsafeCast<ByteArray>()
    }
}
