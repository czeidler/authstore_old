package org.fejoa.auth.crypto

import org.fejoa.auth.*
import org.fejoa.auth.js.launch
import org.w3c.dom.Worker
import org.w3c.dom.events.Event
import kotlin.js.Promise
import kotlin.js.json

/*
class CancelablePromise<out T>(val promise: Promise<T>, val cancelFunction: () -> Unit) {
    companion object {
        fun <T>completedPromise(value: T): CancelablePromise<T> {
            return CancelablePromise(Promise({ resolve, _ -> resolve(value) }), {})
        }
    }

    fun cancel() {
        cancelFunction.invoke()
    }
}*/

class KDFWorker(password: String, kdfParams: KDFParams) {
    private var worker: Worker? = null
    private val promise = CancelablePromise<CryptoKey>(cancelCallback = this::cancel)

    fun get(): CancelablePromise<CryptoKey> {
        return promise
    }

    fun cancel() {
        worker?.terminate()
        promise.setError(Exception("KDF worker canceled"))
    }

    init {
        val worker = Worker("KDFWorker.js")
        this.worker = worker
        worker.onmessage = { event: Event ->
            val data = event.asDynamic().data
            val key = data.key.unsafeCast<CryptoKey>()
            promise.setResult(key)
        }
        worker.onerror = {
            promise.setError(Exception("KDF worker failed"))
        }

        worker.postMessage(json("password" to password,
                "kdfParams" to kdfParams))
    }
}


class BaseKeyCache {
    private val baseKeyMap = HashMap<KDFParams, BaseKey>()

    fun getBaseKey(params: KDFParams): BaseKey? {
        return baseKeyMap[params]
    }

    /**
     * Returns either the cached base key or if not existing derives the key.
     *
     * Newly generated keys are not automatically added to the cache (to avoid caching wrong base keys)
     */
    fun getBaseKey(password: String, params: KDFParams): CancelablePromise<BaseKey> {
        val baseKey = baseKeyMap[params]
        if (baseKey != null)
            return CancelablePromise.completedPromise(baseKey)

        return deriveKey(password, params)
    }

    fun addToCache(params: KDFParams, baseKey: BaseKey) {
        baseKeyMap[params] = baseKey
    }

    fun removeFromCache(params: KDFParams) {
        baseKeyMap.remove(params)
    }

    suspend private fun toBaseKey(key: CryptoKey, params: KDFParams): BaseKey {
        return BaseKey(CryptoJs.toBase64(CryptoJs.keyBytes(key)), params)
    }

    fun generateBaseKey(password: String, algorithm: KDF_ALGO, iterations: Int): CancelablePromise<BaseKey> {
        when (algorithm) {
            KDF_ALGO.PBKDF2WithSHA256 -> {
                val salt = CryptoHelper(CryptoJs).generateSalt16()
                val params = PDKDF2Params(algorithm.name, iterations, CryptoJs.toBase64(salt))
                return deriveKey(password, params)
            }
            else -> throw Exception("BaseKeyCache: Algorithm $algorithm is not supported")
        }
    }

    /**
     * Use a worker thread to calculate the KDF.
     *
     * The worker is cancelable throw the CancelablePromise.
     */
    private fun deriveKey(password: String, params: KDFParams): CancelablePromise<BaseKey> {
        val worker = KDFWorker(password, params)

        val cancelablePromise = CancelablePromise<BaseKey>(cancelCallback =  worker::cancel)
        worker.get().then {key ->
            launch {
                try {
                    cancelablePromise.setResult(toBaseKey(key, params))
                } catch (e: Exception) {
                    cancelablePromise.setError(e)
                }
            }
        }
        return cancelablePromise
    }
}

suspend fun BaseKey.getUserKey(hashAlgo: HASH_ALGO, salt: ByteArray): UserKey {
    CryptoJs.fromBase64(this.key)
    val hexKey = CryptoJs.hex(CryptoJs.fromBase64(this.key))
    val userKey = CryptoJs.hash(TextEncoder().encode(hexKey + CryptoJs.hex(salt)), hashAlgo)
    return UserKey(CryptoJs.toBase64(userKey), UserKeyParams(params, hashAlgo, CryptoJs.toBase64(salt)))
}

suspend fun BaseKey.deriveUserKey(hashAlgo: HASH_ALGO): UserKey {
    val salt = CryptoHelper(CryptoJs).generateSalt16()
    return getUserKey(hashAlgo, salt)
}
