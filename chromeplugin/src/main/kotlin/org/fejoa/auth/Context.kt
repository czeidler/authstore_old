package org.fejoa.auth

import org.fejoa.auth.crypto.BaseKeyCache
import org.fejoa.auth.database.StorageManager
import org.fejoa.auth.passwordmanager.PMAccountManager
import org.fejoa.auth.passwordmanager.PMBackground


class Context {
    val jsonRPC = JsonRPCClient()
    val keyCache = BaseKeyCache()

    val fejoaAuth = FejoaAuth(jsonRPC, keyCache)
    val storageManager = StorageManager(keyCache)
    val accountManager = PMAccountManager(storageManager)
    val pmBackground = PMBackground(accountManager)

    init {
        pmBackground.start()
    }
}