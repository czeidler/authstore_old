package org.fejoa.auth

import org.fejoa.auth.js.launch
import kotlin.browser.window


interface Executor {
    fun run(task: () -> Unit)
}

class NowExecutor : Executor {
    override fun run(task: () -> Unit) {
        task()
    }
}

class AsyncExecutor : Executor {
    override fun run(task: () -> Unit) {
        window.setTimeout({task.invoke()}, 0)
    }
}


class CancelablePromise<T>(executor: Executor = NowExecutor(), private val task: (() -> T)? = null,
                           private val cancelCallback: (() -> Unit)? = null) {
    companion object {
        fun <T>completedPromise(value: T): CancelablePromise<T> {
            val promise = CancelablePromise<T>()
            promise.setResult(value)
            return promise
        }
    }

    private interface Listener<in T> {
        fun onResult(result: T)
        fun onError(error: Throwable)
    }

    interface Result<T>

    class Running<T>: Result<T>
    class Ok<T>(val result: T): Result<T>
    class Error<T>(val error: Throwable): Result<T>

    class CancelationException(message: String) : Exception(message)

    var result: Result<T> = Running()
        private set

    private var parent: CancelablePromise<*>? = null
    private val listeners = ArrayList<Listener<T>>()

    init {
        if (task != null) {
            executor.run {
                try {
                    setResult(task.invoke())
                } catch (e: Throwable) {
                    setError(e)
                }
            }
        }
    }

    fun finished(): Boolean {
        return result !is Running
    }

    fun setResult(result: T) {
        if (finished()) return
        this.result = Ok(result)
        whenCompleted()
    }

    fun setError(error: Throwable) {
        if (finished()) return
        result = Error(error)
        whenCompleted()
    }

    // TODO cancel children even if we already finished?
    fun cancel() {
        if (finished()) return
        setError(CancelationException("Canceled"))
        cancelCallback?.invoke()
        parent?.cancel()
        whenCompleted()
    }

    private fun whenCompleted() {
        listeners.forEach { propagateResult(it) }
        listeners.clear()
    }

    private fun propagateResult(listener: Listener<T>) {
        when (result) {
            is Error -> listener.onError((result as Error).error)
            is Ok -> listener.onResult((result as Ok).result)
        }
    }

    private fun complete(result: Result<T>) {
        this.result = result
        whenCompleted()
    }

    fun <R>bindAsync(executor: Executor, method: (T) -> CancelablePromise<R>): CancelablePromise<R> {
        val resultPromise = CancelablePromise<R>()
        resultPromise.parent = this
        val listener = object: Listener<T> {
            override fun onResult(result: T) {
                executor.run {
                    resultPromise.complete(method.invoke(result).result)
                }
            }

            override fun onError(error: Throwable) {
                if (error is CancelationException)
                    resultPromise.cancel()
                resultPromise.setError(error)
            }
        }
        if (finished()) {
            propagateResult(listener)
        } else {
            listeners.add(listener)
        }
        return resultPromise
    }

    fun <R>thenAsync(executor: Executor, method: (T) -> R): CancelablePromise<R> {
        return bindAsync(executor) {
            val promise = CancelablePromise<R>(cancelCallback = cancelCallback)
            when (result) {
                is Error -> promise.setError((result as Error).error)
                is Ok -> {
                    try {
                        val value = method.invoke((result as Ok).result)
                        promise.setResult(value)
                    } catch (e: Throwable) {
                        promise.setError(e)
                    }
                }
            }
            promise
        }
    }

    fun <R>bind(method: (T) -> CancelablePromise<R>): CancelablePromise<R> {
        return bindAsync(NowExecutor(), method)
    }

    fun <R>then(method: (T) -> R): CancelablePromise<R> {
        return thenAsync(NowExecutor(), method)
    }

    fun <R>thenSuspend(method: suspend (T) -> R): CancelablePromise<R> {
        val promise = CancelablePromise<R>(cancelCallback = cancelCallback)
        whenCompleted { result, error ->
            if (error != null) {
                promise.setError(error)
                return@whenCompleted
            }
            if (result != null) {
                launch {
                    try {
                        val result = method(result)
                        promise.setResult(result)
                    } catch (e: Exception) {
                        promise.setError(e)
                    }
                }
            }
        }
        return promise
    }

    fun whenCompleted(method: (T?, Throwable?) -> Unit) {
        val listener = object: Listener<T> {
            override fun onResult(result: T) {
                method.invoke(result, null)
            }

            override fun onError(error: Throwable) {
                method.invoke(null, error)
            }
        }
        if (finished()) {
            propagateResult(listener)
        } else {
            listeners.add(listener)
        }
    }
}