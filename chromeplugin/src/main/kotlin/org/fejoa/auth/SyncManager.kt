package org.fejoa.auth


class SyncManager {
    class SyncListener(val branch: String, val callback: (branch: String, pulled: Boolean) -> Unit)

    val syncedListeners = ArrayList<SyncListener>()

    private fun notifySyncListeners(syncedBranch: String, pulled: Boolean) {
        syncedListeners.filter { it.branch == syncedBranch }.forEach { it.callback.invoke(syncedBranch, pulled) }
    }

    fun pull(remote: Remote, context: String, branchInfo: BranchInfo) {

    }
}