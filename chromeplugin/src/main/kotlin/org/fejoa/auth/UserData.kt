package org.fejoa.auth

import org.fejoa.auth.crypto.*
import org.fejoa.auth.database.Database
import org.fejoa.auth.js.Object
import kotlin.js.Promise
import kotlin.js.json



class StorageDir(val database: Database, val basePath: String = "") {
    fun child(path: String): StorageDir {
        return StorageDir(database, appendBasePath(path))
    }

    fun getBranch(): String {
        return database.getBranch()
    }

    private fun appendBasePath(path: String): String {
        if (path == "")
            return basePath
        if (basePath == "")
            return path
        return basePath + "/" + path
    }

    fun put(path: String, data: Any) {
        return database.put(appendBasePath(path), data)
    }

    fun get(path: String): Any? {
        return database.get(appendBasePath(path))
    }

    fun getString(path: String): String? {
        return database.getString(appendBasePath(path))
    }

    fun putByteArray(path: String, data: ByteArray) {
        database.putByteArray(appendBasePath(path), data)
    }

    fun getByteArray(path: String): ByteArray? {
        return database.getByteArray(appendBasePath(path))
    }

    fun list(path: String = ""): Array<String> {
        return database.list(appendBasePath(path))
    }

    suspend fun commit() {
        database.commit()
    }
}

class BranchInfo(val branch: String, val description: String, val context: String, val key: CryptoKey) {
    class JsonObject(val branch: String, val description: String, val key: String)

    companion object {
        suspend fun fromJson(jsonObject: JsonObject, context: String): BranchInfo {
            return BranchInfo(jsonObject.branch, jsonObject.description, context,
                    CryptoJs.secretKey(base64js.decode(jsonObject.key), CryptoSettings.Symmetric()))
        }
    }

    suspend fun toJson(): JsonObject {
        return JsonObject(branch, description, base64js.encode(CryptoJs.keyBytes(key)))
    }
}


class Remote(val id: String, val user: String, val url: String) {
    constructor(remote: Remote): this(remote.id, remote.user, remote.url)

    companion object {
        suspend fun create(user: String, url: String): Remote {
            val id = CryptoHelper(CryptoJs).generateId()
            return Remote(id, user, url)
        }
    }
}

open class UserData(val storageDir: StorageDir, id: String?) {
    val ID_KEY = "id"
    val BRANCH_DIR = "branches"
    val REMOTE_DIR = "remotes"

    val onBranchesChangedListeners = ArrayList<() -> Unit>()
    val onRemotesChangedListeners = ArrayList<() -> Unit>()

    init {
        if (id != null)
            setId(id)
    }

    fun getId(): String {
        return storageDir.getString(ID_KEY) ?: ""
    }

    fun setId(id: String) {
        storageDir.put(ID_KEY, id)
    }

    private fun notifyBranchesChanged() {
        onBranchesChangedListeners.forEach { it.invoke() }
    }

    private fun notifyRemotesChanged() {
        onRemotesChangedListeners.forEach { it.invoke() }
    }

    suspend fun addBranch(branchInfo: BranchInfo) {
        storageDir.put("$BRANCH_DIR/" + branchInfo.context + "/" + branchInfo.branch, branchInfo.toJson())
        notifyBranchesChanged()
    }

    suspend fun listBranches(context: String): List<BranchInfo> {
        val contextDir = storageDir.child("$BRANCH_DIR/" + context)
        return contextDir.list().map {
            contextDir.get(it)
        }.filterNotNull().map {
            BranchInfo.fromJson(it.asDynamic(), context)
        }
    }

    suspend fun addRemote(remote: Remote): Boolean {
        if (hasMatchingRemote(remote))
            return false
        storageDir.put("$REMOTE_DIR/" + remote.id, remote)
        notifyRemotesChanged()
        return true
    }

    /**
     * Checks if there is already a remote with the same url and user
     */
    suspend private fun hasMatchingRemote(remote: Remote): Boolean {
        return listRemotes().find { it.user == remote.user && it.url == remote.url } != null
    }

    suspend fun listRemotes(): List<Remote> {
        val remotesDir = storageDir.child(REMOTE_DIR)
        return remotesDir.list().map {
            remotesDir.get(it)
        }.filterNotNull().map {
            it.unsafeCast<Remote>()
        }.map {
            // fill it into a proper object
            Remote(it)
        }
    }

    suspend fun commit() {
        storageDir.commit()
    }
}